//
//  ShapeOptions.swift
//  
//
//  Created by Gowtham on 16/09/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import AVKit

protocol shapeDelegate {
	func didSelectShape(shapeType:ShapeType)
}

class ShapeOptions: NSViewController {
	var shapedelegate:shapeDelegate?
	@IBAction func didDrawRectangle(_ sender: Any) {
		self.shapedelegate?.didSelectShape(shapeType: .rectangle)
	}
	@IBAction func didDrawCircle(_ sender: Any) {
		self.shapedelegate?.didSelectShape(shapeType: .circle)
	}
}
