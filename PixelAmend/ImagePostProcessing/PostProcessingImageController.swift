//
//  PostProcessingImageController.swift
//  
//
//  Created by Gowtham on 30/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa
import AVKit

enum Drawing {
	case freehand, blur, text, arrow, shapes, arrowEditing, drawingStartPointChanged, drawingEndPointChanged, none
}
enum ShapeType {
	case rectangle, circle
}
class PostProcessingImageController: NSViewController, NSTextFieldDelegate, shapeDelegate, arrowDelegate, NSWindowDelegate {
		
	public var recordedImage:URL?
	@IBOutlet weak var savedImage: NSImageView!
	@IBOutlet weak var imageName: NSTextField!
	@IBOutlet weak var shareImage: FlatButton! {
		didSet {
			shareImage.cornerRadius = 5
		}
	}
	@IBOutlet weak var freeHandDraw: FlatButton!{
		didSet {
			freeHandDraw.buttonColor = NSColor.black
			freeHandDraw.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var blurImage: FlatButton!{
		didSet {
			blurImage.buttonColor = NSColor.black
			blurImage.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var drawText: FlatButton!{
		didSet {
			drawText.buttonColor = NSColor.black
			drawText.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var brushSize: FlatButton!{
		didSet {
			brushSize.buttonColor = NSColor.black
			brushSize.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var brushColor: FlatButton!{
		didSet {
			brushColor.buttonColor = NSColor.black
			brushColor.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var drawingArrow: FlatButton!{
		didSet {
			drawingArrow.buttonColor = NSColor.black
			drawingArrow.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var drawShapes: FlatButton!{
		didSet {
			drawShapes.buttonColor = NSColor.black
			drawShapes.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var shapeTrackingView: NSView!
	let lineWeight: CGFloat = 10
	let strokeColor: NSColor = .red
	var currentPath: NSBezierPath?
	var currentShape: CAShapeLayer?
	var currentColor: CurrentColorView!
	var drawingType:Drawing? = .text
	var selectedShapeType:ShapeType? = .none
	var pixelsWidth = 0
	var pixelsHeight = 0
	var trackingTextView:MouseTrackingTextView!
	var texfieldText = "Text"
	var isLastActionIsEnter = false
	var isLastCursorPosition = false
	// MARK: - Life cycle
	override func viewDidLoad() {
		super.viewDidLoad()
		let tempImage = NSImage(byReferencing: recordedImage!)
        savedImage.image =  resizeImage(image: tempImage, maxSize: savedImage.bounds.size)
//        savedImage.image = tempImage
        savedImage.imageScaling = .scaleProportionallyUpOrDown
		imageName.stringValue = recordedImage!.lastPathComponent
		// add tracking on shapeTrackingView
		let options = [NSTrackingArea.Options.mouseMoved, NSTrackingArea.Options.activeInKeyWindow] as NSTrackingArea.Options
		let trackingArea = NSTrackingArea(rect:shapeTrackingView.frame,options:options,owner:self,userInfo:nil)
		shapeTrackingView.addTrackingArea(trackingArea)
		
		let clickGesture = NSClickGestureRecognizer.init(target: self, action: #selector(shapeTrackingClicked(sender:)))
		clickGesture.numberOfClicksRequired = 1
		clickGesture.isEnabled = true
		shapeTrackingView.addGestureRecognizer(clickGesture)

	}
	override func viewDidAppear() {
		view.window?.delegate = self
	}
	
	func windowShouldClose(_ sender: NSWindow) -> Bool {
		self.view.window?.close()
		return true
	}
	// MARK: - Actions
	@IBAction func didStartDrawingFreeHand(_ sender: Any) {
		currentColor = CurrentColorView.newInstance()
		shapeTrackingView.addSubview(currentColor)
		drawingType = .freehand
		
	}
	@IBAction func didDrawBlurInRect(_ sender: Any) {
		let image = NSImage(byReferencing: recordedImage!)
		let rep = image.representations[0]
		pixelsWidth = Int(shapeTrackingView.frame.size.width) - rep.pixelsWide
		pixelsHeight = Int(shapeTrackingView.frame.size.height) - rep.pixelsHigh
		let processedImage = createFilterForDimension(filterRect: CGRect(x: 0, y: 0, width: 200, height: 200))
		let filterView = MouseTrackingImageView.init(frame: CGRect(x: pixelsWidth/2, y: pixelsHeight/2, width: 200, height: 200))
		filterView.image = processedImage
		filterView.trackingImageViewUrl = recordedImage
		filterView.pixelWide = pixelsWidth
		filterView.pixelHeight = pixelsHeight
		filterView.imageScaling = .scaleNone
		shapeTrackingView.addSubview(filterView)
		drawingType = .blur
		
		let clickGesture = NSClickGestureRecognizer.init(target: self, action: #selector(blurImageClicked(sender:)))
		clickGesture.numberOfClicksRequired = 1
		clickGesture.isEnabled = true
		filterView.addGestureRecognizer(clickGesture)

		
	}
	@IBAction func didStartTextWriting(_ sender: Any) {
		trackingTextView = MouseTrackingTextView()
		trackingTextView.frame = CGRect(origin: CGPoint(x: self.view.frame.size.width / 2 , y: self.view.frame.size.height / 2), size: CGSize(width: 100, height: 50))
		trackingTextView.stringValue = "Text"
		trackingTextView.maximumNumberOfLines = 20
		trackingTextView.font = NSFont.boldSystemFont(ofSize: 40)
		trackingTextView.backgroundColor = .clear
		trackingTextView.textColor = .systemBlue
		trackingTextView.drawsBackground = false
		trackingTextView.isBordered = false
		trackingTextView.wantsLayer = true
		trackingTextView.delegate = self
		trackingTextView.lineBreakMode = .byWordWrapping
		trackingTextView.isSelectable = true
		trackingTextView.focusRingType = .none
		let clickGesture = NSClickGestureRecognizer.init(target: self, action: #selector(textFieldClicked(sender:)))
		clickGesture.numberOfClicksRequired = 1
		trackingTextView.addGestureRecognizer(clickGesture)
		
		let editGesture = NSClickGestureRecognizer.init(target: self, action: #selector(textFieldEditClicked(sender:)))
		editGesture.numberOfClicksRequired = 2
		trackingTextView.addGestureRecognizer(editGesture)

		shapeTrackingView.addSubview(trackingTextView)
		drawingType = .text
	}
	func drawCropLineAroundTextfield(label:NSTextField) {
		let cropLine = CAShapeLayer.init()
		cropLine.backgroundColor = .clear
		cropLine.fillColor = NSColor.clear.cgColor
		cropLine.strokeColor = .white
		cropLine.lineWidth = 3
		cropLine.name = "layeredBorder"
		cropLine.lineJoin = CAShapeLayerLineJoin.round
		cropLine.lineDashPattern = [4, 4];
		cropLine.frame = label.bounds
		label.layer?.addSublayer(cropLine)
		cropLine.path = NSBezierPath(rect: label.bounds).cgPath
	}

	public func removeCropLine(textfield:NSTextField) {
		textfield.layer!.sublayers!.forEach { (layer) in
			if layer.name == "layeredBorder" {
				layer.removeFromSuperlayer()
			}
		}
	}
	@IBAction func didUpdateBrushSize(_ sender: Any) {
		let popover = NSPopover.init()
		let popoverContentController = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("BrushStrokeOptions")) as! NSViewController
		popover.contentSize = CGSize(width: 200, height: 100)
		popover.behavior = .transient
		popover.animates = true
		popover.contentViewController = popoverContentController
		let button = sender as? NSButton
		popover.show(relativeTo: button!.bounds, of: button!, preferredEdge: .minX)
	}
	
	@IBAction func didUpdateBrushColor(_ sender: Any) {
		let popover = NSPopover.init()
		let popoverContentController = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("BrushColorOptions")) as! NSViewController
		popover.contentSize = CGSize(width: 200, height: 100)
		popover.behavior = .transient
		popover.animates = true
		popover.contentViewController = popoverContentController
		let button = sender as? NSButton
		popover.show(relativeTo: button!.bounds, of: button!, preferredEdge: .minX)
	}
	
	@IBAction func didDrawArrow(_ sender: Any) {
		drawingType = .arrow
	}
	
	@IBAction func didDrawSelectedShape(_ sender: Any) {
		drawingType = .shapes
		let popover = NSPopover.init()
		let popoverContentController = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil).instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("ShapeOptions")) as! ShapeOptions
		popoverContentController.shapedelegate = self
		popover.contentSize = CGSize(width: 200, height: 100)
		popover.behavior = .transient
		popoverContentController.shapedelegate = self
		popover.animates = true
		popover.contentViewController = popoverContentController
		let button = sender as? NSButton
		popover.show(relativeTo: button!.bounds, of: button!, preferredEdge: .minX)
	}
	//MARK: - Arrow Delegate
	func arrowEditingStarted(type:Drawing) {
		drawingType = type
	}
	func finishedEditing() {
		self.view.bringSubviewToFront(shapeTrackingView)
	}
	// MARK: - shape Delegate
	func didSelectShape(shapeType: ShapeType) {
		selectedShapeType = shapeType
	}

	// MARK: - Methods
	@objc func shapeTrackingClicked(sender:NSClickGestureRecognizer) {
		shapeTrackingView.subviews.forEach { (subView) in
			if (subView.isKind(of: MouseTrackingTextView.self)) {
				let textField = subView as? MouseTrackingTextView
				textField?.layer?.borderColor  = NSColor.clear.cgColor
				textField?.isEditable = false
				textField?.abortEditing()
				textField?.didRemoveCropLine()
			}
		}
	}
	@objc func textFieldClicked(sender:NSClickGestureRecognizer) {
		let label = sender.view as? MouseTrackingTextView
		label?.isEditable = false
		label?.layer?.borderColor = NSColor.clear.cgColor
		label?.initTrackingArea()
		drawCropLineAroundTextfield(label: label!)
	}
	@objc func textFieldEditClicked(sender:NSClickGestureRecognizer) {
		let label = sender.view as? MouseTrackingTextView
		label?.layer?.borderColor = NSColor.red.cgColor
		isLastActionIsEnter = false
		label?.layer?.borderWidth = 2.0
		label?.selectText(nil)
		label?.removeCropLine()
		label?.resizeHandles = []
		label?.isEditable = true
	}
	@objc func blurImageClicked(sender:NSClickGestureRecognizer) {
		let blurredImageObj = sender.view as? MouseTrackingImageView
		blurredImageObj!.drawCropLineAroundBlurImage(bluredImage: blurredImageObj!)
		blurredImageObj?.initTrackingArea()
	}
	
	public func startDrawing(at point: NSPoint) {
	  currentPath = NSBezierPath()
	  currentShape = CAShapeLayer()
	  currentShape?.lineWidth = lineWeight
	  currentShape?.strokeColor = strokeColor.cgColor
	  currentShape?.fillColor = NSColor.clear.cgColor
	  currentShape?.lineJoin = CAShapeLayerLineJoin.round
	  currentShape?.lineCap = CAShapeLayerLineCap.round
	  currentPath?.move(to: point)
	  currentPath?.line(to: point)
	  currentShape?.path = currentPath?.cgPath
	  shapeTrackingView.layer?.addSublayer(currentShape!)
	  currentColor.removeFromSuperview()
	  shapeTrackingView.addSubview(currentColor)

	}
	func continueDrawing(at point: NSPoint) {
	  currentPath?.line(to: point)
	  if let shape = currentShape {
		shape.path = currentPath?.cgPath
	  }
	  updateCurrentColorLocation(point: point)
	}
	func endDrawing(at point: NSPoint) {
	  currentPath?.line(to: point)
	  if let shape = currentShape {
		shape.path = currentPath?.cgPath
	  }
	  currentPath = nil
	  currentShape = nil
	  updateCurrentColorLocation(point: point)
	}
	func updateCurrentColorLocation(point: NSPoint) {
	  currentColor.frame.origin.x = point.x - 20
	  currentColor.frame.origin.y = point.y - 20
	  currentColor.alphaValue = 1
	}
	func createFilterForDimension(filterRect:CGRect) -> NSImage {
		let context = CIContext(options: nil)
		let filter = CIFilter(name: "CIPixellate")
		let logo = CIImage(contentsOf:recordedImage!)
		filter?.setValue(logo, forKey: kCIInputImageKey)
		filter?.setValue(10, forKey:kCIInputScaleKey)
		let result = filter?.value(forKey: kCIOutputImageKey) as! CIImage
		let cgImage = context.createCGImage(result, from: filterRect)
		let processedImage = NSImage(cgImage: cgImage!, size: filterRect.size)
		return processedImage;
	}
	func controlTextDidChange(_ obj: Notification) {
		let field = obj.object as? NSTextField
		removeCropLine(textfield: field!)
		updateText(obj: obj, isEnterKey: false)
	}
	func controlTextDidEndEditing(_ obj: Notification) {
		let userObj = obj.userInfo as? [String: Any]
		let textMovment = userObj!["NSTextMovement"] as? Int
		if (textMovment == NSReturnTextMovement) {
			updateText(obj: obj, isEnterKey: true)
		}
	}
	func control(_ control: NSControl, textView: NSTextView, doCommandBy commandSelector: Selector) -> Bool {
		switch commandSelector {
			case #selector(NSResponder.deleteBackward(_:)):
				if let _ =  control as? NSTextField {
					isLastActionIsEnter = false
				}
				return false
			default:
				// 4. The default returns false, which means that other key operations will not be processed by themselves, but will be handled by the system
			return false
		}
	}
	func updateText(obj: Notification, isEnterKey:Bool) {
		let field = obj.object as? NSTextField
		if (isEnterKey) {
			let currentPosition = field?.currentEditor()?.selectedRange.location
			if (currentPosition! > 0) {
				var currentString = field?.stringValue
				if (currentPosition == field?.stringValue.count) {
					isLastCursorPosition = true
				}
				let _ = currentString!.insert("\n", at: (currentString!.index((currentString!.startIndex), offsetBy: currentPosition!)))
				field!.stringValue = currentString!
				isLastActionIsEnter = true
				texfieldText = field!.stringValue
			}
		} else {
			if (isLastActionIsEnter) {
				isLastActionIsEnter = false
				if (field?.stringValue.last == " " ){
					isLastCursorPosition = false
					field!.stringValue = texfieldText + field!.stringValue
				} else {
					field!.stringValue = isLastCursorPosition ? texfieldText + field!.stringValue : texfieldText
				}
			} else {
				field!.stringValue = field!.stringValue
			}
		}
		let maxWidth: CGFloat = self.view.frame.size.width
		let maxHeight: CGFloat = self.view.frame.size.height
		let size = field!.sizeThatFits(NSSize(width: maxWidth, height: maxHeight))
		field!.frame =  NSRect(x: field!.frame.origin.x, y: field!.frame.origin.y, width: size.width, height: size.height)
		if (isEnterKey) {
			let fieldEditor = field!.currentEditor()
			fieldEditor?.selectedRange = NSMakeRange(0, 0)
		}
	}
    
    func resizeImage(image:NSImage, maxSize:NSSize) -> NSImage {
        var ratio:Float = 0.0
        let imageWidth = Float(image.size.width)
        let imageHeight = Float(image.size.height)
        let maxWidth = Float(maxSize.width)
        let maxHeight = Float(maxSize.height)
        // Get ratio (landscape or portrait)
        if (imageWidth > imageHeight) {
            // Landscape
            ratio = maxWidth / imageWidth;
        } else { // Portrait
            ratio = maxHeight / imageHeight;
        }
        // Calculate new size based on the ratio
        let newWidth = imageWidth * ratio
        let newHeight = imageHeight * ratio
        // Create a new NSSize object with the newly calculated size
        let newSize:NSSize = NSSize(width: Int(newWidth), height: Int(newHeight))
        // Cast the NSImage to a CGImage
        var imageRect:CGRect = NSRect(x: 0, y: 0, width: image.size.width, height: image.size.height)
        let imageRef = image.cgImage(forProposedRect: &imageRect, context: nil, hints: nil)
        // Create NSImage from the CGImage using the new size
        let imageWithNewSize = NSImage(cgImage: imageRef!, size: newSize)
        
        // Return the new image
        return imageWithNewSize
    }

	
	// MARK:  - Mouse life cycle methods
	var startPoint:CGPoint!
	var arrowShape:Arrow?
	var rectangleShape:RectangleShape?
	var circleShape:CircleShape?

	override func mouseMoved(with event: NSEvent) {
		var location = event.locationInWindow;
		switch drawingType {
			case .freehand:
				updateCurrentColorLocation(point: event.locationInWindow)
				break;
		case .arrow:
			NSCursor.crosshair.push()
			location.y = location.y - CGFloat(self.pixelsHeight/2)
			shapeTrackingView.subviews.forEach { (shapeSubView) in
				if (shapeSubView.isKind(of: Arrow.self)) {
					let curvePoint = self.view.convert(event.locationInWindow, to: shapeTrackingView)
					if (shapeSubView.frame.contains(curvePoint)) {
						NSCursor.openHand.push()
					}
				}
			}
			default:
				break
		}
	}
	override func mouseDown(with event: NSEvent) {
		switch drawingType {
			case .freehand:
				startDrawing(at: event.locationInWindow)
				break;
			case .arrow:
				let point = self.view.convert(event.locationInWindow, from: self.view)
				self.startPoint = point;
				if NSCursor.current == NSCursor.openHand {
					
				} else {
					arrowShape = Arrow(frame: shapeTrackingView.frame)
					arrowShape?.addClickGesture()
					arrowShape?.startPoint = self.startPoint
					arrowShape?.endPoint = point
					arrowShape?.arrowDelegate = self
					arrowShape?.wantsLayer = true
					shapeTrackingView.addSubview(arrowShape!)
				}
				break;
			case .shapes:
				let point = self.view.convert(event.locationInWindow, from: self.view)
				switch selectedShapeType {
					case .rectangle :
						rectangleShape = RectangleShape(frame: NSRect(x: point.x, y: point.y, width: 0, height: 0))
						shapeTrackingView.addSubview(rectangleShape!)
						break
					case .circle:
						circleShape = CircleShape(frame: NSRect(x: point.x, y: point.y, width: 0, height: 0))
						shapeTrackingView.addSubview(circleShape!)
						break
					default:
						break
				}
				self.startPoint = point;
				break
			default:
				break
		}
	}
	
	override func mouseDragged(with event: NSEvent) {
		switch drawingType {
			case .freehand:
				continueDrawing(at: event.locationInWindow)
				break;
			case .arrow:
				if NSCursor.current == NSCursor.openHand {
					print("openhand moved")
				} else {
					let point = self.view.convert(event.locationInWindow, from: self.view)
					arrowShape?.startPoint = self.startPoint
					arrowShape?.endPoint = point
					arrowShape?.frame = shapeTrackingView.frame
					arrowShape?.setNeedsDisplay(shapeTrackingView.frame)
				}
				break
			case .shapes:
				let point = self.view.convert(event.locationInWindow, from: self.view)
				let  xPos = min(self.startPoint.x, point.x)
				let yPos = min(self.startPoint.y, point.y)
				let width = fabsf(Float(self.startPoint.x - point.x))
				let height = fabsf(Float(self.startPoint.y - point.y))
				switch selectedShapeType {
					case .circle:
						circleShape?.frame = NSRect(x: xPos, y: yPos, width: CGFloat(width), height: CGFloat(height))
						break
					case .rectangle:
						rectangleShape?.frame = NSRect(x: xPos, y: yPos, width: CGFloat(width), height: CGFloat(height))
						break
					default:
						break
				}
				break
		case .arrowEditing:
			print("mouse dragged arrow editing")
				break
			default:
				break
		}
	}
	override func mouseUp(with event: NSEvent) {
		switch drawingType {
			case .freehand:
				endDrawing(at: event.locationInWindow)
				break;
			case .arrow:
				let arrowShapeLayer =  arrowShape?.layer?.sublayers?.last as? CAShapeLayer
				arrowShape?.frame = (arrowShapeLayer?.path!.boundingBoxOfPath)!
				updateStartAndEndPoints(arrowFrame: arrowShape!.frame)
				break;
			default:
				break
		}
	}
	func updateStartAndEndPoints(arrowFrame:NSRect) {
		let deltaDistance = getLastAvailablePoints()
		let positionDirection = getArrowOrientaion();
		if positionDirection.horizantal == "left" && positionDirection.vertical == "down" { //start at end
			arrowShape!.startPoint = NSPoint(x: deltaDistance.x, y: (arrowShape?.bounds.size.height)!)
			let endXPoint = arrowFrame.size.width - deltaDistance.x;
			arrowShape?.endPoint = NSPoint(x: endXPoint, y: 0)
		} else if positionDirection.horizantal == "left" && positionDirection.vertical == "up" { //start at end
			arrowShape!.startPoint = NSPoint(x: deltaDistance.x, y: 0)
			let endXPoint = arrowFrame.size.width - deltaDistance.x;
			arrowShape?.endPoint = NSPoint(x: endXPoint, y: (arrowShape?.bounds.size.height)!)
		} else if positionDirection.horizantal == "right" && positionDirection.vertical == "up" { //start at end
			let xpos = arrowFrame.size.width + deltaDistance.x
			arrowShape!.startPoint = NSPoint(x: xpos, y: 0)
			arrowShape!.endPoint = NSPoint(x: -deltaDistance.x, y: arrowFrame.size.height)
		} else if positionDirection.horizantal == "right" && positionDirection.vertical == "down" { //start at end
			let xpos = arrowFrame.size.width + deltaDistance.x
			arrowShape!.startPoint = NSPoint(x: xpos, y: arrowFrame.size.height)
			arrowShape!.endPoint = NSPoint(x: -deltaDistance.x, y: 0)
		}
		else if positionDirection.horizantal == "left" && positionDirection.vertical == "equal" { //start at end
			arrowShape!.startPoint = NSPoint(x: arrowFrame.size.width, y: arrowFrame.size.height / 2)
			arrowShape!.endPoint = NSPoint(x: 0, y: arrowFrame.size.height / 2)
		} else if positionDirection.horizantal == "right" && positionDirection.vertical == "equal" { //start at end
			arrowShape!.startPoint = NSPoint(x: 0, y: arrowFrame.size.height / 2)
			arrowShape!.endPoint = NSPoint(x: arrowFrame.size.width, y: arrowFrame.size.height / 2)
		} else if positionDirection.horizantal == "equal" && positionDirection.vertical == "up" { //start at end
			arrowShape!.startPoint = NSPoint(x: arrowFrame.size.width / 2, y: 0)
			arrowShape!.endPoint = NSPoint(x: arrowFrame.size.width / 2, y: arrowFrame.size.height)
		} else if positionDirection.horizantal == "equal" && positionDirection.vertical == "down" { //start at end
			arrowShape!.startPoint = NSPoint(x: arrowFrame.size.width / 2, y: arrowFrame.size.height)
			arrowShape!.endPoint = NSPoint(x: arrowFrame.size.width / 2, y: 0)
		}
		arrowShape?.setNeedsDisplay(shapeTrackingView.frame)
	}
	func invalidateRect(shape:NSView) {
		self.view.setNeedsDisplay(shapeTrackingView.frame)
	}
	func getLastAvailablePoints() -> DeltaPoints {
		let diffX = (self.arrowShape?.startPoint.x)! - (self.arrowShape?.endPoint.x)!
		let diffY = (self.arrowShape?.startPoint.y)! - (self.arrowShape?.endPoint.y)!
		return DeltaPoints(x:diffX, y:diffY)
	}
	func getArrowOrientaion() -> ValidateDirection {
		//vertically
		var verticalPostion = "";
		var horizantalPosition = "";
		if (arrowShape!.startPoint.y > arrowShape!.endPoint.y) { //vertically down
			verticalPostion = "down"
		}
		if (arrowShape!.endPoint.y > arrowShape!.startPoint.y ) { //vertically up
			verticalPostion = "up"
		}
		//horizantally
		if arrowShape!.startPoint.x > arrowShape!.endPoint.x { // horizantally left
			horizantalPosition = "left"
		}
		if arrowShape!.endPoint.x > arrowShape!.startPoint.x { //horizantally right
			horizantalPosition = "right"
		}
		// equal
		if arrowShape!.endPoint.y == arrowShape!.startPoint.y {
			verticalPostion = "equal"
		}
		if arrowShape!.endPoint.x == arrowShape!.startPoint.x {
			horizantalPosition = "equal"
		}
		return ValidateDirection(vertical: verticalPostion, horizantal: horizantalPosition)
	}
}

final class MouseTrackingTextView: NSTextField, NSTextFieldDelegate, handleDelegate {
	
    // MARK: - Lifecycle
	private var cursorType: NSCursor {
		return isEditable ? .iBeam : .openHand
	}
	private var currentTrackingArea: NSTrackingArea?
	var activeHandle:ResizeHandle?
	var handlePosition = Array(repeating: NSRect.zero, count: 8)
	var resizeHandles:[ResizeHandle]! = (NSMutableArray.init(capacity: 8) as! [ResizeHandle])

	func initTrackingArea() {
        setupTrackingArea()
		setUpHandle()
	}
	func setUpHandle() {
		if(self.resizeHandles.count == 0){
			self.initHandles();
		}
		positionHandlesForRect(cropRect: self.frame, isHandleExists: true)
	}
    func superviewResized() {
        resetTrackingArea()
    }
    // MARK: - MouseTrackingTextView Mouse Events
    override func resetCursorRects() {
        addCursorRect(bounds, cursor: cursorType)
    }
	override func mouseEntered(with event: NSEvent) {
		cursorType.set()
	}
	
	override func mouseDown(with event: NSEvent) {
		let location = event.locationInWindow;
		let point = self.convert(location, from: self)
		for rect in self.handlePosition {
			if (rect.contains(point)) {
				let selectedhandle = self.handlePosition.firstIndex(of: rect)
				self.activeHandle = self.resizeHandles[selectedhandle!]
			}
		}
	}
	
	override func mouseMoved(with event: NSEvent) {
		let location = event.locationInWindow;
		let point = self.convert(location, from: self)
		var cursorPosition = false
		for rect in self.handlePosition {
			if (rect.contains(point)) {
				let selectedhandle = self.handlePosition.firstIndex(of: rect)
				if (self.resizeHandles.count > 0) {
					let handle = self.resizeHandles[selectedhandle!]
					handle.getCursorPosition().push()
					cursorPosition = true
				}
			}
		}
		if (cursorPosition) {
			return;
		}
		cursorType.set()
	}
	
	override func mouseDragged(with event: NSEvent) {
		if((self.activeHandle) != nil) {
			self.activeHandle?.setRepresentedPoint(representedPoint: event.locationInWindow)
		} else {
			let deltaPoint = CGPoint(x: self.frame.origin.x + event.deltaX, y: self.frame.origin.y - event.deltaY)
			self.frame = NSRect(x:deltaPoint.x, y: deltaPoint.y, width: self.frame.size.width, height: self.frame.size.height)
		}
	}
	
	override func mouseUp(with event: NSEvent) {
		resetTrackingArea()
		self.activeHandle = nil;
	}
	public func didRemoveCropLine() {
		if ((self.activeHandle) == nil) {
			removeCropLine()
			self.resizeHandles = []
		}
	}
	override func mouseExited(with event: NSEvent) {
		NSCursor.arrow.push()

	}
	
    // MARK: - Private API
    private func setupTrackingArea() {
		let trackingArea = NSTrackingArea(rect: bounds, options: [.activeAlways, .mouseMoved, .mouseEnteredAndExited], owner: self, userInfo: nil)
        currentTrackingArea = trackingArea
		self.addTrackingArea(trackingArea)
    }
    private func resetTrackingArea() {
        if let trackingArea = currentTrackingArea {
            removeTrackingArea(trackingArea)
        }
        setupTrackingArea()
    }
	public func removeCropLine() {
		self.layer?.sublayers?.forEach { (layer) in
			if layer.name == "layeredBorder" {
				layer.removeFromSuperlayer()
			} else if (layer.name == "resizeHandle") {
				layer.removeFromSuperlayer()
			}
		}
	}
	
	//handles
	func handle(sender: ResizeHandle, point: CGPoint) {
		if (sender.resizeLocation == .ResizePositionLeft) {
			self.frame.size.width = self.frame.size.width - (point.x - self.frame.minX);
			self.frame.origin.x = point.x;
		}
		if (sender.resizeLocation == .ResizePositionRight) {
			self.frame.size.width = self.frame.size.width + (point.x - self.frame.size.width - self.frame.minX);
		}
		if (sender.resizeLocation == .ResizePositionLeft || sender.resizeLocation == .ResizePositionRight) {
			self.layer?.sublayers?.forEach({ (borderdLayer) in
				if (borderdLayer.name == "layeredBorder") {
					let layeredBorder = borderdLayer as? CAShapeLayer
					layeredBorder!.frame = self.bounds
					layeredBorder!.path = NSBezierPath(rect: self.bounds).cgPath
				}
			})
		}
	}
	func initHandles() {
		for index in  0...7 {
			let position = ResizePosition.init(rawValue: index)
			let handle = ResizeHandle.handleWithPosition(position: position!) as? ResizeHandle
			handle?.backgroundColor = NSColor.white.cgColor
			handle?.handledelegate = self
			handle?.name = "resizeHandle"
			self.layer?.addSublayer(handle!)
			self.resizeHandles.append(handle!)
		}
	}
	func positionHandlesForRect(cropRect: CGRect, isHandleExists:Bool) {
		for handle in self.resizeHandles {
			handle.active = isHandleExists
			switch handle.resizeLocation {
				case .ResizePositionTopLeft:
					handle.frame.size = .zero
					handle.position = CGPoint(x:cropRect.origin.x + 5, y:cropRect.origin.y + cropRect.size.height - 8);
					self.handlePosition[0] = NSRect.init(x: handle.position.x , y: handle.position.y , width: 0, height: 0)
					break;
				case .ResizePositionTop:
					handle.frame.size = .zero
					handle.position = CGPoint(x:cropRect.origin.x + (cropRect.size.width / 2), y:cropRect.origin.y + cropRect.size.height);
					self.handlePosition[1] = NSRect.init(x: handle.position.x, y: handle.position.y, width: 0, height: 0)
				  break;
				case .ResizePositionTopRight:
					handle.position = CGPoint(x:cropRect.origin.x + cropRect.size.width, y:cropRect.origin.y + cropRect.size.height);
					handle.frame.size = .zero
					self.handlePosition[2] = NSRect.init(x: handle.position.x, y: handle.position.y, width: 0, height: 0)
					  break;
				case .ResizePositionLeft:
					handle.position = CGPoint(x:0, y:self.frame.size.height / 2);
					self.handlePosition[3] = NSRect.init(x: cropRect.origin.x, y: cropRect.origin.y + (cropRect.size.height / 2) - 8, width: 8, height: 8)
					  break;
				case .ResizePositionRight:
					handle.position = CGPoint(x:self.frame.size.width , y:self.frame.size.height / 2);
					self.handlePosition[4] = NSRect.init(x: (cropRect.origin.x + cropRect.size.width) - 8, y: cropRect.origin.y + (cropRect.size.height / 2) - 8, width: 8, height: 8)
					  break;
				case .ResizePositionBottomLeft:
					handle.position = CGPoint(x:cropRect.origin.x, y:cropRect.origin.y);
					handle.frame.size = .zero
					self.handlePosition[5] = NSRect.init(x: handle.position.x, y: handle.position.y, width: 0, height: 0)
					  break;
				case .ResizePositionBottom:
					handle.frame.size = .zero
					handle.position = CGPoint(x:cropRect.origin.x + (cropRect.size.width / 2), y:cropRect.origin.y);
					self.handlePosition[6] = NSRect.init(x: handle.position.x, y: handle.position.y, width: 0, height: 0)
					  break;
				case .ResizePositionBottomRight:
					handle.frame.size = .zero
					handle.position = CGPoint(x:cropRect.origin.x + cropRect.size.width, y:cropRect.origin.y);
					self.handlePosition[7] = NSRect.init(x: handle.position.x, y: handle.position.y, width: 0, height: 0)
					  break;
				  default:
					  break;
			}
		}
	}
}

final class MouseTrackingImageView: NSImageView, handleDelegate {
	
    // MARK: - Lifecycle
	private var currentTrackingArea: NSTrackingArea?
	public var trackingImageViewUrl:URL?
	public var drawingType:Drawing?
	public var pixelWide: Int?
	public var pixelHeight: Int?
	private var cursorType: NSCursor {
		return isEditable ? .iBeam : .openHand
	}
	var resizeHandles:[ResizeHandle]! = (NSMutableArray.init(capacity: 8) as! [ResizeHandle])
	var activeHandle:ResizeHandle?
	var handlePosition = Array(repeating: NSRect.zero, count: 8)
	
	func drawCropLineAroundBlurImage(bluredImage:MouseTrackingImageView) {
		let cropLine = CAShapeLayer.init()
		cropLine.backgroundColor = .clear
		cropLine.fillColor = NSColor.clear.cgColor
		cropLine.strokeColor = .white
		cropLine.lineWidth = 3
		cropLine.name = "blurImagelayeredBorder"
		cropLine.lineJoin = CAShapeLayerLineJoin.round
		cropLine.lineDashPattern = [6, 6];
		cropLine.frame = self.bounds
		self.layer?.addSublayer(cropLine)
		cropLine.path = NSBezierPath(rect: NSRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)).cgPath
	}

	
	func createFilterForDimension(filterRect:CGRect) -> NSImage? {
		let context = CIContext(options: nil)
		let filter = CIFilter(name: "CIPixellate")
		let logo = CIImage(contentsOf:self.trackingImageViewUrl!)
		filter?.setValue(logo, forKey: kCIInputImageKey)
		filter?.setValue(10, forKey:kCIInputScaleKey)
		let result = filter?.value(forKey: kCIOutputImageKey) as! CIImage
		let cgImage = context.createCGImage(result, from: filterRect)
		var processedImage : NSImage?
		if (cgImage != nil) {
			processedImage = NSImage(cgImage: cgImage!, size: filterRect.size)
		}
		return processedImage;
	}

	public func initTrackingArea() {
        setupTrackingArea()
		setUpHandle()
	}
    func superviewResized() {
        resetTrackingArea()
    }
    // MARK: - Mouse Events
    override func resetCursorRects() {
        addCursorRect(bounds, cursor: cursorType)
    }
	
	// MARK: - Mouse Exited
	override func mouseDown(with event: NSEvent) {
		var location = event.locationInWindow;
		location.x = location.x + 4
		location.y = location.y - CGFloat(self.pixelHeight!/2)
		for rect in self.handlePosition {
			if (rect.contains(location)) {
				let selectedhandle = self.handlePosition.firstIndex(of: rect)
				self.activeHandle = self.resizeHandles[selectedhandle!]
			}
		}
	}
	
    override func mouseMoved(with event: NSEvent) {
		var location = event.locationInWindow;
		location.x = location.x + 4
		location.y = location.y - CGFloat(self.pixelHeight!/2)
		var cursorPosition = false
		for rect in self.handlePosition {
			print("point", self.handlePosition[3], location)
			if (rect.contains(location)) {
				let selectedhandle = self.handlePosition.firstIndex(of: rect)
				if (self.resizeHandles.count > 0) {
					let handle = self.resizeHandles[selectedhandle!]
					handle.getCursorPosition().push()
					cursorPosition = true
				}
			}
		}
		if (cursorPosition) {
			return;
		}
        cursorType.set()
    }
	override func mouseUp(with event: NSEvent) {
		resetTrackingArea()
		self.activeHandle = nil;
	}
	override func mouseDragged(with event: NSEvent) {
		if((self.activeHandle) != nil) {
			self.activeHandle?.setRepresentedPoint(representedPoint: event.locationInWindow)
		} else {
			let deltaPoint = CGPoint(x: self.frame.origin.x + event.deltaX, y: self.frame.origin.y - event.deltaY)
			self.frame = NSRect(x:deltaPoint.x, y: deltaPoint.y, width: self.frame.size.width, height: self.frame.size.height)
			let image = createFilterForDimension(filterRect: NSRect(x: deltaPoint.x - CGFloat(pixelWide! / 2), y: deltaPoint.y - CGFloat(pixelHeight! / 2), width: self.frame.size.width, height: self.frame.size.height))
			let yPos = deltaPoint.y - CGFloat(pixelHeight! / 2)
			self.positionHandlesForRect(cropRect: NSRect(x: self.frame.origin.x, y: yPos, width: self.frame.size.width, height: self.frame.size.height), isHandleExists: true)
			if ((image) != nil){
				self.image = image!
			}
		}
	}
    // MARK: - Private API
    private func setupTrackingArea() {
		let trackingArea = NSTrackingArea(rect: bounds, options: [.activeAlways, .mouseMoved, .mouseEnteredAndExited], owner: self, userInfo: nil)
        currentTrackingArea = trackingArea
		self.addTrackingArea(trackingArea)
    }
	func setUpHandle() {
		if(self.resizeHandles.count == 0){
			self.initHandles();
		}
		positionHandlesForRect(cropRect: self.frame, isHandleExists: true)
	}
	func initHandles() {
		for index in  0...7 {
			let position = ResizePosition.init(rawValue: index)
			let handle = ResizeHandle.handleWithPosition(position: position!) as? ResizeHandle
			handle?.handledelegate = self
			handle?.name = "resizeHandleImageView"
			self.layer?.addSublayer(handle!)
			self.resizeHandles.append(handle!)
		}
	}
	public func didRemoveCropLine() {
		if ((self.activeHandle) == nil) {
			removeCropLine()
			self.resizeHandles = []
		}
	}
	func removeCropLine() {
		self.layer?.sublayers?.forEach { (layer) in
			if layer.name == "blurImagelayeredBorder" {
				layer.removeFromSuperlayer()
			} else if (layer.name == "resizeHandleImageView") {
				layer.removeFromSuperlayer()
			}
		}
	}
    private func resetTrackingArea() {
        if let trackingArea = currentTrackingArea {
            removeTrackingArea(trackingArea)
        }
        setupTrackingArea()
    }
	func createBlurFilter(){
		let image = createFilterForDimension(filterRect: NSRect(x: self.frame.origin.x, y: self.frame.origin.y - CGFloat(self.pixelHeight! / 2), width: self.frame.size.width, height: self.frame.size.height))
		if ((image) != nil){
			self.image = image!
		}
		self.layer?.sublayers?.forEach({ (borderdLayer) in
			if (borderdLayer.name == "blurImagelayeredBorder") {
				let layeredBorder = borderdLayer as? CAShapeLayer
				layeredBorder!.frame = self.bounds
				layeredBorder!.path = NSBezierPath(rect: self.bounds).cgPath
			}
		})
	}
	func handle(sender: ResizeHandle, point: CGPoint) {
		if (sender.resizeLocation == .ResizePositionTop) {
			 self.frame.size.height = point.y - self.frame.origin.y
			return self.createBlurFilter()
		}
		if (sender.resizeLocation == .ResizePositionBottom){
			self.frame.size.height = self.frame.size.height - (point.y - self.frame.origin.y);
			self.frame.origin.y    = point.y;
			return self.createBlurFilter()

		}
		if (sender.resizeLocation == .ResizePositionRight) {
			 self.frame.size.width = self.frame.size.width + (point.x - self.frame.size.width - self.frame.minX);
			return self.createBlurFilter()

		}
		if (sender.resizeLocation == .ResizePositionLeft) {
			self.frame.size.width = self.frame.size.width - (point.x - self.frame.minX);
			 self.frame.origin.x = point.x;
		}
		if (sender.resizeLocation == .ResizePositionTopLeft || sender.resizeLocation == .ResizePositionBottomLeft) {
			self.frame.size.width = self.frame.width + (self.frame.minX - point.x);
			self.frame.origin.x   = point.x;
		} else {
			self.frame.size.width -= self.frame.maxX - point.x;
		}
		if (sender.resizeLocation == .ResizePositionTopLeft || sender.resizeLocation == .ResizePositionTopRight) {
			self.frame.size.height = self.frame.height - (self.frame.maxY - point.y);
			self.frame.origin.y    = point.y - self.frame.height;
		} else {
			self.frame.size.height = self.frame.height + (self.frame.minY - point.y);
			self.frame.origin.y    = point.y;
		}
		return self.createBlurFilter()
	}

	func positionHandlesForRect(cropRect: CGRect, isHandleExists:Bool) {
		for handle in self.resizeHandles {
			handle.active = isHandleExists
			switch handle.resizeLocation {
				case .ResizePositionTopLeft:
					handle.position = CGPoint(x:0 , y:self.frame.size.height);
					let yPos = (cropRect.origin.y + cropRect.size.height) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[0] = NSRect.init(x: cropRect.origin.x , y: yPos, width: 8, height: 8)
					break;
				case .ResizePositionTop:
					handle.position = CGPoint(x:(self.frame.size.width/2) , y:self.frame.size.height);
					let yPos = (cropRect.origin.y + cropRect.size.height) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[1] = NSRect.init(x: cropRect.origin.x + (cropRect.size.width / 2),  y: yPos, width: 8, height: 8)
				  break;
				case .ResizePositionTopRight:
					handle.position = CGPoint(x:self.frame.size.width , y:self.frame.size.height);
					let yPos = (cropRect.origin.y + cropRect.size.height) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[2] = NSRect.init(x: cropRect.origin.x + cropRect.size.width - 4, y: yPos, width: 8, height: 8)
					  break;
				case .ResizePositionLeft:
					handle.position = CGPoint(x:0, y:self.frame.size.height / 2);
					let yPos = (cropRect.origin.y + (cropRect.size.height/2)) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[3] = NSRect.init(x: cropRect.origin.x, y: yPos, width: 8, height: 8)
					  break;
				case .ResizePositionRight:
					handle.position = CGPoint(x:self.frame.size.width , y:self.frame.size.height / 2);
					let yPos = (cropRect.origin.y + (cropRect.size.height/2)) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[4] = NSRect.init(x: (cropRect.origin.x + cropRect.size.width) - 8, y: yPos, width: 8, height: 8)
					  break;
				case .ResizePositionBottomLeft:
					handle.position = CGPoint(x:0, y:0);
					let yPos = (cropRect.origin.y) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[5] = NSRect.init(x: cropRect.origin.x, y: yPos, width: 8, height: 8)
					  break;
				case .ResizePositionBottom:
					handle.position = CGPoint(x:self.frame.size.width / 2 , y:0 );
					let yPos = (cropRect.origin.y) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[6] = NSRect.init(x: cropRect.origin.x + (cropRect.size.width / 2), y: yPos, width: 8, height: 8)
					  break;
				case .ResizePositionBottomRight:
					handle.position = CGPoint(x:self.frame.size.width , y:0);
					let yPos = (cropRect.origin.y) - CGFloat(self.pixelHeight!/2) - 4
					self.handlePosition[7] = NSRect.init(x: (cropRect.origin.x + cropRect.size.width) - 8, y: yPos, width: 8, height: 8)
					  break;
				  default:
					  break;
			}
		}
	}

}
