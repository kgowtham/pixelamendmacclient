//
//  AudiorecordingWindow.swift
//  
//
//  Created by Gowtham on 14/09/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa

class AudioRecordingWindow: NSViewController {
	
	@IBOutlet weak var recordingTime: NSTextField!
	@IBOutlet weak var recordAudio: NSButton!
	var timer : Timer!
	var  currentTime = 1
	var speechRecorderObj : SpeechRecorder?
	
	@objc func timerAction(timer: Timer) {
		currentTime = currentTime + 1
		let time = secondsToHoursMinutesSeconds(seconds: currentTime)
		recordingTime.stringValue = String(format: "%02d", time.1) + ":" + String(format: "%02d", time.2)
	}
	func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
	  return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
	}
	@IBAction func didStartAudioRecording(_ sender: Any) {
		let recording = sender as? NSButton
		if recording?.tag == 0 {
			timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction(timer:)), userInfo: nil, repeats: true)
			recording?.tag = 1
			recording!.title = "Stop Recording"
			speechRecorderObj = SpeechRecorder()
			speechRecorderObj!.start()
		} else {
			timer.invalidate()
			timer = nil
			recording?.tag = 0
			recording!.title = "Start Recording"
			speechRecorderObj?.stop()
		}
	}
	override func viewDidLoad() {
		super.viewDidLoad()
		
	}
}
