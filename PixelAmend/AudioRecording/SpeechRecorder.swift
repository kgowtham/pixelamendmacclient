
import Foundation
import AVFoundation
import Cocoa

open class SpeechRecorder: NSObject {
  private var destinationUrl: URL!

  var recorder: AVAudioRecorder?
  let player = AVQueuePlayer()

  open func start() {
    destinationUrl = createUniqueOutputURL()
    do {
      let format = AVAudioFormat(settings: [
        AVFormatIDKey: kAudioFormatMPEG4AAC,
        AVEncoderAudioQualityKey: AVAudioQuality.high,
        AVSampleRateKey: 44100.0,
        AVNumberOfChannelsKey: 1,
        AVLinearPCMBitDepthKey: 16,
        ])!
      let recorder = try AVAudioRecorder(url: destinationUrl, format: format)
	  recorder.record()
      self.recorder = recorder
    } catch let error {
      let _ = (error as NSError).code
    }
  }
  open func stop() {
    if let recorder = recorder {
		recorder.stop()
		player.removeAllItems()
		player.insert(AVPlayerItem(url: destinationUrl), after: nil)
		player.play()
	}
  }
  func createUniqueOutputURL() -> URL {
	let filePath = self.captureTemporaryFilePath()
	let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filePath)
    destinationUrl = fileUrl
    return fileUrl
  }
	func captureTemporaryFilePath() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let timeFormatter = DateFormatter()
		timeFormatter.dateFormat = "HH-mm-ss"
		let now = Date()
		let date = dateFormatter.string(from: now)
		let time = timeFormatter.string(from: now)
		let fileName = NSString.init(format: "%@%@%@.%@",  "ScreenCaptureRecording", date, time, "m4a")
		return fileName as String
	}

}
