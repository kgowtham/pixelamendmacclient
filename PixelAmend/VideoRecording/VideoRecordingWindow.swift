//
//  VideoRecordingWindow.swift
//
//  Created by Gowtham on 14/09/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa
import AVKit

class VideoRecordingWindow: NSViewController, AVCaptureFileOutputRecordingDelegate {
	
	var captureFaceTimeSession = AVCaptureSession()
    var captureDevice : AVCaptureDevice?
    var previewLayer : AVCaptureVideoPreviewLayer?
	@IBOutlet var faceTimeCamera:NSView?
	var movieFileOutput: AVCaptureMovieFileOutput!
    var audioCaptureDevice : AVCaptureDevice?
	var timer : Timer!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.faceTimeCamera?.wantsLayer = true
		setUpFaceTimeCamera()
	}
	func setUpFaceTimeCamera() {
		if (!captureFaceTimeSession.isRunning) {
			let devices = AVCaptureDevice.devices()
			for device in devices {
				if ((device as AnyObject).hasMediaType(AVMediaType.video)) {
					captureDevice = device
				}
				if ((device as AnyObject).hasMediaType(AVMediaType.audio)) {
					audioCaptureDevice = device
				}
			}
			if captureDevice != nil {
				do {
					try captureFaceTimeSession.addInput(AVCaptureDeviceInput(device: captureDevice!))
					previewLayer = AVCaptureVideoPreviewLayer(session: captureFaceTimeSession)
					previewLayer?.frame = (self.faceTimeCamera!.layer?.bounds)!
					previewLayer?.backgroundColor = .clear
					previewLayer?.videoGravity = .resizeAspectFill
					self.faceTimeCamera!.layer?.addSublayer(previewLayer!)
					captureFaceTimeSession.startRunning()
				} catch {
					print(AVCaptureSessionErrorKey.description)
				}
			}
		}
				
		do {
			try captureFaceTimeSession.addInput(AVCaptureDeviceInput(device: audioCaptureDevice!))
		} catch {
			print(AVCaptureSessionErrorKey.description)
		}
		
		movieFileOutput = AVCaptureMovieFileOutput.init()
		if self.captureFaceTimeSession.canAddOutput(movieFileOutput) {
			self.captureFaceTimeSession.addOutput(movieFileOutput)
		}
	}
	
	func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
		print("stopped recording", error as Any)
		captureFaceTimeSession.stopRunning();
	}
	func captureTemporaryFilePath() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let timeFormatter = DateFormatter()
		timeFormatter.dateFormat = "HH-mm-ss"
		let now = Date()
		let date = dateFormatter.string(from: now)
		let time = timeFormatter.string(from: now)
		let fileName = NSString.init(format: "%@%@%@.%@",  "ScreenCaptureRecording", date, time, "mov")
		return fileName as String
	}
	
	@objc func timerAction(timer: Timer) {
		
	}
	@IBAction func didStartVideoRecording(_ sender: Any) {
		let recording = sender as? NSButton
		if recording?.tag == 0 {
			timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction(timer:)), userInfo: nil, repeats: true)
			recording?.tag = 1
			recording!.title = "Stop Recording"
			
			captureFaceTimeSession.startRunning()
			let filePath = self.captureTemporaryFilePath()
			if FileManager.default.fileExists(atPath: filePath) {
				do {
					try FileManager.default.removeItem(atPath: filePath)
				} catch  {
					
				}
			}
			let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filePath)
			self.movieFileOutput.startRecording(to:  fileUrl, recordingDelegate: self)

		} else {
			timer.invalidate()
			timer = nil
			recording?.tag = 0
			recording!.title = "Start Recording"
			movieFileOutput.stopRecording()
		}
	}
}
