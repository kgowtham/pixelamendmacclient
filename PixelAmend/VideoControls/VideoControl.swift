//
//  VideoControl.swift
//  
//
//  Created by Gowtham on 20/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa
class VideoControl: NSView {
	override func draw(_ dirtyRect: NSRect) {
		let mainPath = NSBezierPath.init(roundedRect: self.bounds, xRadius: 5, yRadius: 5)
		mainPath.setClip()
		NSColor.controlBackgroundColor.setFill()
		NSColor.unemphasizedSelectedContentBackgroundColor.setStroke()
		_ = NSRect.fill(self.bounds)
		mainPath.lineWidth = 1.0
		mainPath.stroke()
	}
}
