//
//  VideoControlsViewController.swift
//  
//
//  Created by Gowtham on 14/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa
import AVFoundation

class VideoControlsViewController: NSViewController {
	
	@IBOutlet weak var sessionOptionsMenu : NSMenu!
	@IBOutlet weak var recordButton: NSButton!
	var videoGranted:Bool?
	var audioGranted:Bool?
	enum CaptureOptions : Int {
		case TimerThree = 1, TimerFive, TimerTen, ShowMouseClicks
	}
	
	override func viewDidLoad() {
		super.viewDidLoad()
		self.view.wantsLayer = true
		self.view.layer?.backgroundColor = NSColor.black.cgColor
		let userSelection = UserSharedPreferences.shared.fetchUserPreferences()
		let captureType  = userSelection?.value(forKey: "captureType") as? String
		if (captureType == "screenRecording") {
			recordButton.title = "Record"
		} else {
			recordButton.title = "Capture"
		}
	}
	@IBAction func cancelSession(_ sender: Any) {
		self.view.window?.close()
	}
	@IBAction func setSessionOptions(_ sender: NSPopUpButtonCell) {
		let item  = sender.selectedItem
		switch (item!.tag) {
			case CaptureOptions.TimerThree.rawValue:
				UserSharedPreferences.shared.saveUserPreferences(key: "startDelay", value: 3)
			   updateSessionsUI()
				break;
			case CaptureOptions.TimerFive.rawValue:
				 UserSharedPreferences.shared.saveUserPreferences(key: "startDelay", value: 5)
				updateSessionsUI()
				break;
			case CaptureOptions.TimerTen.rawValue:
				UserSharedPreferences.shared.saveUserPreferences(key: "startDelay", value: 10)
				updateSessionsUI()
				break;
			case CaptureOptions.ShowMouseClicks.rawValue:
				let userSelection = UserSharedPreferences.shared.fetchUserPreferences()
				let showMouseClicks = userSelection?.value(forKey: "showMouseClicks") as? Bool
				UserSharedPreferences.shared.saveUserPreferences(key: "showMouseClicks", value: !showMouseClicks!)
				updateSessionsUI()
				break;
			default:
				break
		}
	}
	func updateSessionsUI() {
		let userSelection = UserSharedPreferences.shared.fetchUserPreferences()
		let startDelay = userSelection?.value(forKey: "startDelay") as? Int
		let showMouseClicks = userSelection?.value(forKey: "showMouseClicks") as? Bool
		self.sessionOptionsMenu.item(withTag: 1)?.state = startDelay == 3 ?  .on : .off
		self.sessionOptionsMenu.item(withTag: 2)?.state = startDelay == 5 ?  .on : .off
		self.sessionOptionsMenu.item(withTag: 3)?.state = startDelay == 10 ? .on : .off
		self.sessionOptionsMenu.item(withTag: 4)?.state = showMouseClicks! ? .on : .off

	}
	@IBAction func beginRecording(sender:Any) {
		requestVideoPermissionComplete { (completion) in
			self.requestAudioPermission { (audioCompletion) in
				DispatchQueue.main.async {
					UserSharedPreferences.shared.saveUserPreferences(key: "isRecording", value: true)
					NotificationCenter.default.post(Notification(name: .startRecording,object: nil))
				}
			}
		}
	}
	func requestVideoPermissionComplete(callback: @escaping(_ result:Bool) -> Void){
		let status = AVCaptureDevice.authorizationStatus(for: .video)
		self.videoGranted = status == AVAuthorizationStatus.authorized;
		if self.videoGranted! {
			callback(true)
		} else {
			if status as AnyObject !==  AVAuthorizationStatus.authorized as AnyObject {
				DispatchQueue.main.async {
					self.view.window?.ignoresMouseEvents = true
					self.view.window?.level = .normal
				}
				AVCaptureDevice.requestAccess(for: .video) { (granted) in
					DispatchQueue.main.async {
						self.view.window?.ignoresMouseEvents = false
						self.view.window?.level = NSWindow.Level.statusBar
					}
					self.videoGranted = granted
					if(!granted){
						DispatchQueue.main.async {
							let alert = NSAlert.init()
							alert.messageText = "Error trying to record your screen"
							alert.informativeText = "Please allow access to record your screen under System Preferences > Security & Privacy > Privacy > Camera.";
							alert.runModal()
						}
					} else {
						callback(true)
					}
				}
			}
		}
	}
	
	func requestAudioPermission(callback : @escaping (_ result : Bool) -> Void) {
		let status = AVCaptureDevice.authorizationStatus(for: .audio)
		self.audioGranted = status == AVAuthorizationStatus.authorized;
		if self.audioGranted! {
			callback(true)
		} else {
			if status as AnyObject !==  AVAuthorizationStatus.authorized as AnyObject {
				DispatchQueue.main.async {
					self.view.window?.ignoresMouseEvents = true
					self.view.window?.level = .normal
				}
				AVCaptureDevice.requestAccess(for: .audio) { (granted) in
					DispatchQueue.main.async {
						self.view.window?.ignoresMouseEvents = false
						self.view.window?.level = NSWindow.Level.statusBar
					}
					self.audioGranted = granted
					if(!granted){
					   DispatchQueue.main.async {
						   let alert = NSAlert.init()
						   alert.messageText = "Error trying to record your screen"
						   alert.informativeText = "Please allow access to record your screen under System Preferences > Security & Privacy > Privacy > Camera.";
						   alert.runModal()
					   }
				   } else {
					   callback(true)
				   }
				}
			}
		}
	}
}
