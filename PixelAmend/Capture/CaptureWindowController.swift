//
//  CaptureWindowController.swift
//  
//
//  Created by Gowtham on 14/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa

class CaptureWindowController: NSWindowController {
	override init(window: NSWindow?) {
		let panel = CapturePanel.init(contentRect: NSScreen.main!.frame, styleMask: [.borderless, .fullSizeContentView], backing: .buffered, defer: false)
		super.init(window: panel)
		self.window?.setFrame(NSScreen.main!.frame, display: true)
		self.showWindow(self)
		panel.becomeKey()
		NSApp.activate(ignoringOtherApps: true)
	}
	override var acceptsFirstResponder: Bool {
		return true
	}
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
