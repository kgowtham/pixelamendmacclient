//
//  ResizeHandle.swift
//  
//
//  Created by Gowtham on 15/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Cocoa
import AVFoundation
import CoreGraphics
let DEFAULT_HANDLE_WIDTH = 8;

enum ResizePosition : Int{
	case ResizePositionTopLeft = 0,
	ResizePositionTop,
    ResizePositionTopRight,
	ResizePositionLeft,
    ResizePositionRight,
	ResizePositionBottomLeft,
	ResizePositionBottom,
	ResizePositionBottomRight,
	ResizePositionCount
}
protocol handleDelegate {
	func handle(sender:ResizeHandle, point:CGPoint)
}
class  ResizeHandle : CAShapeLayer {
	
	var cursor:NSCursor? {
		get {
			getCursorPosition()
		}
	}
	var selected:Bool?
	var active:Bool?
	var representedPoint:CGPoint?
	var handledelegate:handleDelegate?
	var resizeLocation:ResizePosition?
	

	static func handleWithPosition(position: ResizePosition) -> Any {
		let handle = ResizeHandle.init(layer: self)
		handle.resizeLocation = position
		handle.frame  = CGRect(x: 0, y: 0, width: CGFloat(DEFAULT_HANDLE_WIDTH), height: CGFloat(DEFAULT_HANDLE_WIDTH))
		handle.cornerRadius  = 4;
		return handle
	}
	override init(layer: Any) {
		super.init(layer: layer)
	}
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	override func contains(_ p: CGPoint) -> Bool {
		let thickPath = self.path?.copy(strokingWithWidth: 10, lineCap: .butt, lineJoin: .bevel, miterLimit: 0)
		if((thickPath) != nil) {
			let containsPoint = thickPath?.contains(p)
			return containsPoint!
		}
		return false
	}
	func getRepresentedPoint() -> CGPoint {
		return representedPoint!
	}
	
	func setRepresentedPoint(representedPoint: CGPoint) {
		self.representedPoint = representedPoint;
		CATransaction.begin()
		CATransaction.disableActions()
		self.position =  CGPoint(x: representedPoint.x, y: representedPoint.y)
		CATransaction.commit()
		self.handledelegate?.handle(sender: self, point: representedPoint)
	}
	func path() -> CGPath{
		return NSBezierPath.init(ovalIn: self.bounds) as! CGPath
	}
	func getCursorPosition() -> NSCursor {
		print("self.resizeLocation", self.resizeLocation!);
		switch self.resizeLocation {
			case .ResizePositionTop:
				return NSCursor.init(image: NSImage.init(named: "resizenorthsouth")!, hotSpot: NSPoint(x: 0, y: 0))
			case .ResizePositionBottom:
				return NSCursor.init(image: NSImage.init(named: "resizenorthsouth")!, hotSpot: NSPoint(x: 0, y: 0))
			case .ResizePositionLeft:
				return NSCursor.init(image: NSImage.init(named: "resizeeastwest")!, hotSpot: NSPoint(x: 0, y: 0))
			case .ResizePositionRight:
				return NSCursor.init(image: NSImage.init(named: "resizeeastwest")!, hotSpot: NSPoint(x: 0, y: 0))
			case .ResizePositionBottomLeft:
				return NSCursor.init(image: NSImage.init(named: "resizenortheastsouthwest")!, hotSpot: NSPoint(x: 0, y: 0))
			case .ResizePositionTopRight:
				return NSCursor.init(image: NSImage.init(named: "resizenortheastsouthwest")!, hotSpot: NSPoint(x: 0, y: 0))
			case .ResizePositionTopLeft:
				return NSCursor.init(image: NSImage.init(named: "resizenorthwestsoutheast")!, hotSpot: NSPoint(x: 0, y: 0))
			case .ResizePositionBottomRight:
				return NSCursor.init(image: NSImage.init(named: "resizenorthwestsoutheast")!, hotSpot: NSPoint(x: 0, y: 0))
			default:
				return NSCursor.init(image: NSImage.init(named: "resizenorthsouth")!, hotSpot: NSPoint(x: 0, y: 0))
		}
	}
}
