//
//  CaptureSessionOptions.swift
//  
//
//  Created by Gowtham on 20/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa
import AVFoundation

class CaptureSessionOptions : NSObject {

	static var shared = CaptureSessionOptions()
	var captureRect:CGRect?
	var startDelay:Int?
	var disableAudioRecording:Bool?
	var showMouseClicks:Bool?
	var defaultMicID:String?
	var mic:AVCaptureDevice?
}
