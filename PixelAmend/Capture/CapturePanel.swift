//
//  CapturePanel.swift
//  
//
//  Created by Gowtham on 14/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa
import AVKit

var _cropRect:CGRect?
class CapturePanelBackgroundView : NSView , handleDelegate{
	var backgroundColorLayer:CALayer?
	var cropLine:CAShapeLayer?
	var resizeHandles:[ResizeHandle]!
	var handlePosition:[NSRect]!
	var startPoint:CGPoint!
	var liveResize = false
	var activeHandle:ResizeHandle?
	var startRect:CGRect?
	
	override init(frame frameRect: NSRect) {
		super.init(frame: frameRect)
		self.wantsLayer = true
		self.autoresizingMask = [.width, .height]
		self.autoresizesSubviews = true
		self.backgroundColorLayer = CALayer.init()
		
		self.backgroundColorLayer!.frame = NSRect(x: 0, y: 0, width: frameRect.size.width, height: frameRect.size.height);
		self.backgroundColorLayer?.bounds = self.bounds
		self.backgroundColorLayer?.backgroundColor = NSColor.init(white: 0.0, alpha: 1).cgColor
		self.layer?.addSublayer(self.backgroundColorLayer!)
		self.resizeHandles = (NSMutableArray.init(capacity: 8) as! [ResizeHandle])
		self.handlePosition = Array(repeating: NSRect.zero, count: 8)
	}
	override func acceptsFirstMouse(for event: NSEvent?) -> Bool {
		return true
	}
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
	
	func setCropRect(cropRect:NSRect) {
		if(cropRect.size.height == 0 || cropRect.size.width == 0) {
			return;
		}
		_cropRect = cropRect;
		CATransaction.begin()
		CATransaction.disableActions()
		var maskLayer:CAShapeLayer
		if((self.layer!.mask) != nil) {
			maskLayer = self.layer!.mask as! CAShapeLayer;
		} else {
			maskLayer = CAShapeLayer.init()
		}
		let maskPath = CGMutablePath()
		maskPath.addRect(self.bounds)
		let cropRectPath = CGPath(rect: cropRect, transform: nil)
		maskPath.addPath(cropRectPath)
		maskLayer.path = maskPath
		maskLayer.fillRule = CAShapeLayerFillRule.evenOdd
		self.backgroundColorLayer?.mask = maskLayer
		
		if !(self.cropLine != nil) {
			self.cropLine = CAShapeLayer.init()
			self.cropLine?.backgroundColor = .clear
			self.cropLine?.fillColor = .clear
			self.cropLine?.strokeColor = .white
			self.cropLine?.lineWidth = 1
			self.cropLine?.lineJoin = CAShapeLayerLineJoin.round
			self.cropLine?.lineDashPattern = [4, 4];
			self.layer?.addSublayer(self.cropLine!)
		}
		let linePath = CGPath.init(rect: NSRect.init(x: cropRect.origin.x - 1, y: cropRect.origin.y - 1, width: cropRect.size.width + 2, height: cropRect.size.height + 2), transform: nil)
		self.cropLine?.path = linePath
		var isHandleExists = true;
		if(self.resizeHandles.count == 0){
			self.initHandles();
			isHandleExists = false;
		}
		CATransaction.commit()
		self.positionHandlesForRect(cropRect: cropRect, isHandleExists: isHandleExists)
		
		
	}
	func initHandles() {
		for index in  0...7 {
			let position = ResizePosition.init(rawValue: index)
			let handle = ResizeHandle.handleWithPosition(position: position!) as? ResizeHandle
			handle?.backgroundColor = NSColor.white.cgColor
			handle?.handledelegate = self
			self.layer?.addSublayer(handle!)
			self.resizeHandles.append(handle!)
		}
	}
	 func setFrame(_ frameRect: NSRect, display flag: Bool) {
		super.frame = frameRect
		self.updateTrackingAreas()
		
	}
	override func updateTrackingAreas() {
		super.updateTrackingAreas()
		self.addTrackingArea(NSTrackingArea.init(rect: self.frame, options: [.mouseMoved, .activeAlways, .mouseEnteredAndExited], owner: self, userInfo: nil));
	}
	
	func moveCropRectFromPoint(point:CGPoint, newPoint:CGPoint) {
		let difference = CGPoint(x: newPoint.x - point.x, y: newPoint.y - point.y)
		var newRect = self.startRect;
		newRect  = newRect?.applying(CGAffineTransform(translationX: difference.x, y: difference.y))
		self.setCropRect(cropRect: newRect!)
	}
	func handle(sender: ResizeHandle, point: CGPoint) {

		var bounding = _cropRect;
		if (sender.resizeLocation == .ResizePositionTop) {
			let initialY = bounding!.origin.y;
			bounding!.size.height = point.y - initialY;
			bounding!.origin.y    = initialY;
			return self.setCropRect(cropRect: bounding!)
		}
		if (sender.resizeLocation == .ResizePositionBottom){
			bounding!.size.height = bounding!.size.height - (point.y - bounding!.origin.y);
			bounding!.origin.y    = point.y;
			return self.setCropRect(cropRect: bounding!)
		}
		if (sender.resizeLocation == .ResizePositionRight){
			bounding!.size.width = bounding!.width + (point.x - bounding!.width - bounding!.minX);
			return self.setCropRect(cropRect: bounding!)
		}
		if (sender.resizeLocation == .ResizePositionLeft){
			bounding!.size.width = bounding!.width - (point.x - bounding!.minX);
			bounding!.origin.x = point.x;
			return self.setCropRect(cropRect: bounding!)
		}

		
		if (sender.resizeLocation == .ResizePositionTopLeft || sender.resizeLocation == .ResizePositionBottomLeft) {
			bounding!.size.width = bounding!.width + (bounding!.minX - point.x);
			bounding!.origin.x   = point.x;
		}else{
			bounding!.size.width -= bounding!.maxX - point.x;
		}

		if (sender.resizeLocation == .ResizePositionTopLeft || sender.resizeLocation == .ResizePositionTopRight) {
			bounding!.size.height = bounding!.height - (bounding!.maxY - point.y);
			bounding!.origin.y    = point.y - bounding!.height;
		} else {
			bounding!.size.height = bounding!.height + (bounding!.minY - point.y);
			bounding!.origin.y    = point.y;
		}
		self.setCropRect(cropRect: bounding!)
	}
	
	func positionHandlesForRect(cropRect: CGRect, isHandleExists:Bool) {
		for handle in self.resizeHandles {
			handle.active = isHandleExists
			switch handle.resizeLocation {
				case .ResizePositionTopLeft:
					handle.position = CGPoint(x:cropRect.origin.x, y:cropRect.origin.y + cropRect.size.height);
					self.handlePosition[0] = NSRect.init(x: handle.position.x - 4 , y: handle.position.y - 4, width: 8, height: 8)
					break;
				case .ResizePositionTop:
					handle.position = CGPoint(x:cropRect.origin.x + (cropRect.size.width / 2), y:cropRect.origin.y + cropRect.size.height);
					self.handlePosition[1] = NSRect.init(x: handle.position.x - 4 , y: handle.position.y, width: 8, height: 8)
					break;
				case .ResizePositionTopRight:
					handle.position = CGPoint(x:cropRect.origin.x + cropRect.size.width, y:cropRect.origin.y + cropRect.size.height);
					self.handlePosition[2] = NSRect.init(x: handle.position.x - 4, y: handle.position.y, width: 8, height: 8)
					  break;
				case .ResizePositionLeft:
					handle.position = CGPoint(x:cropRect.origin.x, y:cropRect.origin.y + (cropRect.size.height / 2));
					self.handlePosition[3] = NSRect.init(x: handle.position.x - 4, y: handle.position.y - 4 , width: 8, height: 8)
					  break;
				case .ResizePositionRight:
					handle.position = CGPoint(x:cropRect.origin.x + cropRect.size.width, y:cropRect.origin.y + (cropRect.size.height / 2));
					self.handlePosition[4] = NSRect.init(x: handle.position.x - 4, y: handle.position.y, width: 8, height: 8)
					  break;
				case .ResizePositionBottomLeft:
					handle.position = CGPoint(x:cropRect.origin.x, y:cropRect.origin.y);
					self.handlePosition[5] = NSRect.init(x: handle.position.x - 4, y: handle.position.y - 4, width: 8, height: 8)
					  break;
				case .ResizePositionBottom:
					handle.position = CGPoint(x:cropRect.origin.x + (cropRect.size.width / 2), y:cropRect.origin.y);
					self.handlePosition[6] = NSRect.init(x: handle.position.x - 4, y: handle.position.y, width: 8, height: 8)
					  break;
				case .ResizePositionBottomRight:
					handle.position = CGPoint(x:cropRect.origin.x + cropRect.size.width, y:cropRect.origin.y);
					self.handlePosition[7] = NSRect.init(x: handle.position.x - 4, y: handle.position.y, width: 8, height: 8)
					  break;
				  default:
					  break;
			}
		}
	}
	
	
	override func mouseMoved(with event: NSEvent) {
		let location = event.locationInWindow;
		let point = self.convert(location, from: nil)
		var cursorPosition = false
		for rect in handlePosition {
			if (rect.contains(point)) {
				let selectedhandle = handlePosition.firstIndex(of: rect)
				let handle = self.resizeHandles[selectedhandle!]
				handle.getCursorPosition().push()
				cursorPosition = true
			}
		}
		if (cursorPosition) {
			return;
		} else if ((_cropRect!.contains(point))) {
			NSCursor.openHand.push()
			return;
		} else {
			NSCursor.crosshair.push()
		}
	}
	override func mouseDragged(with event: NSEvent) {
		let point = self.convert(event.locationInWindow, from: nil)
		if (self.liveResize) {
			let  xPos = min(self.startPoint.x, point.x)
			let yPos = min(self.startPoint.y, point.y)
			let width = fabsf(Float(self.startPoint.x - point.x))
			let height = fabsf(Float(self.startPoint.y - point.y))
			let liveRect = NSRect.init(x: xPos, y: yPos, width: CGFloat(width), height: CGFloat(height))
			self.setCropRect(cropRect: liveRect)
			return;
		}
		if((self.activeHandle) != nil) {
			self.activeHandle?.setRepresentedPoint(representedPoint: point)
		} else if ((_cropRect?.contains(point)) != nil){
			self.moveCropRectFromPoint(point: self.startPoint, newPoint: point)
		}
	}
	override func mouseDown(with event: NSEvent) {
		var activeHandle = false;
		let point = self.convert(event.locationInWindow, from: nil)
		for rect in handlePosition {
			if (rect.contains(point)) {
				let selectedhandle = handlePosition.firstIndex(of: rect)
				self.activeHandle = self.resizeHandles[selectedhandle!]
				activeHandle = true
			}
		}
		if(activeHandle) {
		} else if(_cropRect!.contains(point))  {
			self.startPoint = point;
			self.startRect = _cropRect;
		 } else  {
			self.liveResize = true;
			self.startPoint = point;
		}
	}
	override func mouseUp(with event: NSEvent) {
		self.activeHandle = nil;
		self.liveResize = false;
	}
}

class CapturePanel : NSPanel {
	var backgroundColorLayer:CALayer?
	var startedRecording = false
	var bgView: CapturePanelBackgroundView?
	static  let kCropRectKey = "CropRectKey";
	var cropRect:NSRect?
	var controls: VideoControlsViewController?
	var captureSession: CaptureSession?
    var captureDevice : AVCaptureDevice?
    var previewLayer : AVCaptureVideoPreviewLayer?
	var captureFaceTimeSession = AVCaptureSession()
	var faceTimeCamera:NSView?
	func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
		DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
			completion()
		}
	}
	deinit {
		NotificationCenter.default.removeObserver(self, name: .startRecording, object: nil)
		NotificationCenter.default.removeObserver(self, name: .endRecording, object: nil)

	}
	override init(contentRect: NSRect, styleMask style: NSWindow.StyleMask, backing backingStoreType: NSWindow.BackingStoreType, defer flag: Bool) {
		super.init(contentRect: NSZeroRect, styleMask: style, backing: backingStoreType, defer: flag)
		self.isFloatingPanel = true
		self.collectionBehavior  = [.canJoinAllSpaces, .fullScreenAuxiliary]
		self.backgroundColor = .clear
		self.isMovableByWindowBackground = false
		self.isExcludedFromWindowsMenu = true
		self.alphaValue = 1.0
		self.isOpaque = false
		self.hasShadow = false
		self.hidesOnDeactivate = false
		self.level = NSWindow.Level.normal
		self.isRestorable = false
		self.disableSnapshotRestoration()
		self.ignoresMouseEvents = false
		self.bgView = CapturePanelBackgroundView.init(frame: contentRect)
		self.bgView?.autoresizingMask = [.width, .height]
		self.contentView?.layer?.backgroundColor = .black
		self.contentView?.addSubview(self.bgView!)
		let userPreferences = UserSharedPreferences.shared.fetchUserPreferences()
		let recordingMode  = userPreferences?.value(forKey: "recordingMode") as? String
		let captureType  = userPreferences?.value(forKey: "captureType") as? String
		let recordingType  = userPreferences?.value(forKey: "recordingType") as? String
		let initalRect = recordingMode == "fullScreen" ? contentRect : NSRect(x: 10, y: 84, width: contentRect.size.width - 20, height: contentRect.size.height - 160)
		self.bgView?.setCropRect(cropRect: initalRect)
		if (captureType == "screenRecording" && recordingType == "screenCam") {
			self.faceTimeCamera = NSView.init(frame: NSRect.init(x: initalRect.origin.x, y: initalRect.origin.y, width: 200, height: 200))
			self.faceTimeCamera?.layer = CALayer()
			self.faceTimeCamera?.wantsLayer = true
			self.faceTimeCamera?.layer?.masksToBounds = true
			self.faceTimeCamera?.layer?.cornerRadius = 100.0
			self.contentView?.addSubview(self.faceTimeCamera!)
		}
		self.controls = VideoControlsViewController.init(nibName: "VideoControlsViewController", bundle: nil)
		self.controls?.view.wantsLayer = true
		self.contentView?.addSubview(self.controls!.view)
		self.positionControls()
		NotificationCenter.default.addObserver(self, selector: #selector(startRecording(note:)), name: .startRecording, object: nil)
		NotificationCenter.default.addObserver(self, selector: #selector(stopRecording(note:)), name: .endRecording, object: nil)

	}
	func captureWindowRect(initalRect:CGRect) {
		let filePath = captureTemporaryFilePath()
		let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filePath)
		let img = CGDisplayCreateImage(CGMainDisplayID(), rect: initalRect)
		let dest = CGImageDestinationCreateWithURL(fileUrl as CFURL, kUTTypePNG, 1, nil)
		CGImageDestinationAddImage(dest!, img!, nil)
		CGImageDestinationFinalize(dest!)
		openPostProcessingImageController(imageUrl: fileUrl)
		UserSharedPreferences.shared.saveUserPreferences(key: "isRecording", value: false)
	}
	
	@objc func timerAction(timer: Timer) {
		let captureInfo = timer.userInfo as? CaptureStartTimer
		let currentTime = Int(captureInfo!.timerText.stringValue)!
		captureInfo!.timerText.stringValue = String(currentTime - 1)
		if currentTime == 0 {
			timer.invalidate()
			let userSelection = UserSharedPreferences.shared.fetchUserPreferences()
			let captureType  = userSelection?.value(forKey: "captureType") as? String
			captureInfo?.removeFromSuperview()
			if (captureType == "screenRecording") {
				if !startedRecording {
					startedRecording = true
					captureSession = CaptureSession.init()
					self.setUpFaceTimeCamera()
					CaptureSessionOptions.shared.captureRect = _cropRect
					captureSession?.beginRecordingWithOptions(options: CaptureSessionOptions.shared)
				}
			} else {
				DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [weak self] in
					self!.captureWindowRect(initalRect: (self!.bgView?.cropLine?.path!.boundingBox)!)
				}
			}
		}
	}
	
	@objc func startRecording(note: NSNotification) {
		self.ignoresMouseEvents = true
		self.bgView?.isHidden = true
		self.controls?.view.isHidden = true
		let appdelegate = NSApplication.shared.delegate as? AppDelegate
		appdelegate?.closePopover(sender: nil)
		let userSelection = UserSharedPreferences.shared.fetchUserPreferences()
		var startDelay = userSelection?.value(forKey: "startDelay") as? Int
		startDelay =  startDelay == 0 ? 3 : startDelay
		let captureStartTimer = CaptureStartTimer()
		self.faceTimeCamera?.frame = CGRect(x: _cropRect!.origin.x, y: _cropRect!.origin.y, width: 200, height: 200)
		captureStartTimer.frame = CGRect(x: self.frame.size.width / 2 - 100, y: self.frame.size.height / 2 - 100, width: 200, height: 200)
		captureStartTimer.wantsLayer = true
		captureStartTimer.layer?.backgroundColor = NSColor(red: 0, green: 0, blue: 0, alpha: 0.7).cgColor
		self.contentView?.addSubview(captureStartTimer)
		captureStartTimer.timerText.stringValue = String(startDelay!)
		Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction(timer:)), userInfo: captureStartTimer, repeats: true)
	}
	@objc func stopRecording(note: NSNotification) {
		captureFaceTimeSession.stopRunning()
		captureSession?.endRecording()
	}
	override func setFrame(_ frameRect: NSRect, display flag: Bool) {
		super.setFrame(frameRect, display: flag)
	}
	func positionControls() {
		self.controls?.view.frame =  NSRect(x: (self.frame.size.width / 2) - self.controls!.view.frame.size.width / 2, y: 105, width: self.controls!.view.frame.size.width, height: self.controls!.view.frame.size.height)
	}
	func setUpFaceTimeCamera() {
		if (!captureFaceTimeSession.isRunning) {
			let devices = AVCaptureDevice.devices()
			for device in devices {
				if ((device as AnyObject).hasMediaType(AVMediaType.video)) {
					captureDevice = device
				}
			}
			if captureDevice != nil {
				do {
					try captureFaceTimeSession.addInput(AVCaptureDeviceInput(device: captureDevice!))
					previewLayer = AVCaptureVideoPreviewLayer(session: captureFaceTimeSession)
					previewLayer?.frame = (self.faceTimeCamera!.layer?.bounds)!
					previewLayer?.backgroundColor = .clear
					previewLayer?.videoGravity = .resizeAspectFill
					self.faceTimeCamera!.layer?.addSublayer(previewLayer!)
					captureFaceTimeSession.startRunning()
				} catch {
					print(AVCaptureSessionErrorKey.description)
				}
			}
		}
	}
	func captureTemporaryFilePath() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let timeFormatter = DateFormatter()
		timeFormatter.dateFormat = "HH-mm-ss"
		let now = Date()
		let date = dateFormatter.string(from: now)
		let time = timeFormatter.string(from: now)
		let fileName = NSString.init(format: "%@%@%@.%@",  "ScreenCaptureRecording", date, time, "png")
		return fileName as String
	}
	func openPostProcessingImageController(imageUrl:URL) {
		let xPos = 400
		let yPos = 100
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("PostProcessingImageController")
		let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? PostProcessingImageController
		viewcontroller?.recordedImage = imageUrl
		let mainWindow = NSWindow(contentRect: NSMakeRect(CGFloat(xPos), CGFloat(yPos), 500, NSScreen.main!.frame.size.height - 200), styleMask: [.titled, .closable, .fullScreen, .miniaturizable], backing: .buffered, defer: false)
//		mainWindow.titlebarAppearsTransparent = true
		mainWindow.contentViewController = viewcontroller
		mainWindow.makeKeyAndOrderFront(mainWindow)
		NSApp.activate(ignoringOtherApps: true)

	}
}
