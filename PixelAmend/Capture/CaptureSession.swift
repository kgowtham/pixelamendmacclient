//
//  CaptureSession.swift
//  
//
//  Created by Gowtham on 21/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import AVFoundation
import Cocoa
import CoreGraphics

class CaptureSession: NSObject, AVCaptureFileOutputRecordingDelegate {
	
	var  recordingSession : AVCaptureSession!
	var movieFileOutput: AVCaptureMovieFileOutput!
	
	func getCaptureDevice() -> AVCaptureDevice {
		let userSelection = UserSharedPreferences.shared.fetchUserPreferences()
		let selectedMicId = userSelection?.value(forKey: "defaultMicId") as? String
		let devices = AVCaptureDevice.devices()
		for device in devices {
			if ((device as AVCaptureDevice).hasMediaType(AVMediaType.audio)) {
				if device.localizedName == selectedMicId {
					return device;
				}
			}
		}
		return AVCaptureDevice(uniqueID: "")!;
	}
	func beginRecordingWithOptions(options:CaptureSessionOptions) {
		
		recordingSession = AVCaptureSession.init()
		recordingSession.sessionPreset = .high
		var displayId  = CGMainDisplayID()
		let screenNumber = NSScreen.main?.deviceDescription[NSDeviceDescriptionKey(rawValue: "NSScreenNumber")]
		if ((screenNumber) != nil) {
			displayId = NSScreen.main?.deviceDescription[NSDeviceDescriptionKey(rawValue: "NSScreenNumber")] as! CGDirectDisplayID
		}
		let input  = AVCaptureScreenInput.init(displayID: displayId)
		input?.cropRect = options.captureRect!
		let userSelection = UserSharedPreferences.shared.fetchUserPreferences()
		let showMouseClicks = userSelection?.value(forKey: "showMouseClicks") as? Bool
		input?.capturesMouseClicks = showMouseClicks!
		if self.recordingSession.canAddInput(input!) {
			self.recordingSession.addInput(input!)
		}
		movieFileOutput = AVCaptureMovieFileOutput.init()
		if self.recordingSession.canAddOutput(movieFileOutput) {
			self.recordingSession.addOutput(movieFileOutput)
		}
		let micId = userSelection?.value(forKey: "defaultMicId") as? String
		if micId != nil && micId != "Mute"{
			do {
				let captureDevice = getCaptureDevice()
				let audioInput = try AVCaptureDeviceInput.init(device: captureDevice)
				if recordingSession.canAddInput(audioInput) {
					recordingSession.addInput(audioInput)
				}
			} catch  {
				print("error");
			}
		}
		recordingSession.startRunning()
		let filePath = self.captureTemporaryFilePath()
		if FileManager.default.fileExists(atPath: filePath) {
			do {
				try FileManager.default.removeItem(atPath: filePath)
			} catch  {
				
			}
		}
		let fileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filePath)
		self.movieFileOutput.startRecording(to:  fileUrl, recordingDelegate: self)
	}
	
	func captureTemporaryFilePath() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let timeFormatter = DateFormatter()
		timeFormatter.dateFormat = "HH-mm-ss"
		let now = Date()
		let date = dateFormatter.string(from: now)
		let time = timeFormatter.string(from: now)
		let fileName = NSString.init(format: "%@%@%-@.%@",  "ScreenCaptureRecording", date, time, "mov")
		return fileName as String
	}
	func endRecording() {
		movieFileOutput.stopRecording()
	}
	func fileOutput(_ output: AVCaptureFileOutput, didFinishRecordingTo outputFileURL: URL, from connections: [AVCaptureConnection], error: Error?) {
		recordingSession.stopRunning();
		didOpenPostProcessingVideo(outputFileURL: outputFileURL)
	}
	func didOpenPostProcessingVideo(outputFileURL:URL){
		let xPos = 200
		let yPos = 100
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("PostProcessingVideoController")
		let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? PostProcessingVideoController
		viewcontroller?.recordedVideo = outputFileURL
		let mainWindow = NSWindow(contentRect: NSMakeRect(CGFloat(xPos), CGFloat(yPos), NSScreen.main!.frame.size.width - 400, NSScreen.main!.frame.size.height - 200), styleMask: [.titled, .resizable, .miniaturizable, .closable, .fullScreen], backing: .buffered, defer: false)
		mainWindow.titlebarAppearsTransparent = true
		mainWindow.contentViewController = viewcontroller
		mainWindow.makeKeyAndOrderFront(mainWindow)
	}
	func saveOutputAsGIF(outputFileURL:URL) {
		GIFConverter.createGIFFromSource(outputFileURL, frameCount: 24, delayTime: 0.5, loopCount: 7) { (result) in
		}
	}
}
