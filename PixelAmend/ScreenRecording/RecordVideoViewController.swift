//
//  RecordVideoViewController.swift
//
//
//  Created by Gowtham on 13/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//


import Cocoa
import AVKit

protocol recordingPreferences {
	func didSavePreference(key:String, value:String)
}
class RecordVideoViewController: NSViewController {
	@IBOutlet weak var audioDevicesDropdown: NSPopUpButton!
	@IBOutlet weak var videoDevicesDropdown: NSPopUpButton!
	@IBOutlet weak var startVideoRecording: FlatButton!{
		didSet {
			startVideoRecording.cornerRadius = 5
		}
	}
	@IBOutlet weak var screenTypeSegment: NSSegmentedControl!
	let segmentSelectedColor : NSColor = NSColor(red: 0/255.0, green: 125/255.0, blue: 255/255.0, alpha: 1)
	let segmentUnselectedColor : NSColor = NSColor.init(red: 33.0/255.0, green: 33.0/255.0, blue: 33.0/255.0, alpha: 1)
	var audioDevices = [String:AVCaptureDevice]()
	var videoDevices = [String:AVCaptureDevice]()
	var recordPreferenceObj:recordingPreferences?

	override func viewDidLoad() {
		super.viewDidLoad()
		let devices = AVCaptureDevice.devices()
		for device in devices {
			if ((device as AnyObject).hasMediaType(AVMediaType.audio)) {
				audioDevices[device.localizedName] = device
				audioDevicesDropdown.addItem(withTitle: device.localizedName)
			} else if ((device as AnyObject).hasMediaType(AVMediaType.video)) {
				videoDevices[device.localizedName] = device
				videoDevicesDropdown.addItem(withTitle: device.localizedName)
			}
		}
		audioDevicesDropdown.addItem(withTitle: "Mute")
	}
	@IBAction func didStartVideoRecording(_ sender: Any) {
		let _ = CaptureWindowController.init()
		self.view.window?.close()
	}
	@IBAction func dismissRecordViewController(_ sender: Any) {
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("ViewController")
		guard let viewController = storyboard.instantiateController(withIdentifier: identifier) as? ViewController else {
		  fatalError("Why cant i find ViewController? - Check Main.storyboard")
		}
		view.window?.contentViewController = viewController
	}
	@IBAction func didSelectFullScreen(_ sender:Any) {
		self.recordPreferenceObj?.didSavePreference(key: "recordingMode", value: "fullScreen")
	}
	@IBAction func didSelectScreenRegion(_ sender:Any) {
		self.recordPreferenceObj?.didSavePreference(key: "recordingMode", value: "selectedRegion")
	}
	@IBAction func popUpSelectionDidChange(_ sender: NSPopUpButton) {
		self.recordPreferenceObj?.didSavePreference(key: "defaultMicId", value: sender.titleOfSelectedItem!)
	}
	@IBAction func didChangeSegmentControl(_ sender: Any) {
		let segemnt = sender as! NSSegmentedControl
		let segmentTitle = segemnt.selectedSegment == 0 ? "screen" : "screenCam"
		self.recordPreferenceObj?.didSavePreference(key: "recordingType", value: segmentTitle)
	}
}
