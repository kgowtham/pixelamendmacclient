//
//  CaptureStartTimer.swift
//  PixelAmend
//
//  Created by Gowtham on 31/10/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import AVKit

class CaptureStartTimer : NSView, LoadableView {
	var mainView: NSView?
	
	@IBOutlet weak var timerText: NSTextField!
	init() {
		super.init(frame: NSRect.zero)
		_ = load(fromNIBNamed: "CaptureStartTimer")
	}
	
	
	@IBAction func didCancelTimer(_ sender: Any) {
	}
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}
