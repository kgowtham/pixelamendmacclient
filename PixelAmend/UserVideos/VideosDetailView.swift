//
//  PurchasesDetailView.swift
//  TableDemo
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import Cocoa

class VideosDetailView: NSView, LoadableView {
    
    // MARK: - IBOutlet Properties
    
    @IBOutlet weak var videoName: NSTextField!
    @IBOutlet weak var avatarImageView: NSImageView!
    
    // MARK: - Properties
    var mainView: NSView?
	
    // MARK: - Init
    init() {
        super.init(frame: NSRect.zero)
        _ = load(fromNIBNamed: "VideosDetailView")
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
