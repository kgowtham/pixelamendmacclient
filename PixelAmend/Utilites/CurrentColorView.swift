//
//  CurrentColorView.swift
//  
//
//  Created by Gowtham on 31/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Cocoa

class CurrentColorView: NSView {
  static func newInstance() -> CurrentColorView{
    let view = CurrentColorView(frame: NSRect(origin: CGPoint(), size: CGSize(width: 14, height: 14)))
    view.layer = CALayer()
    view.layer?.backgroundColor = NSColor.red.cgColor
    view.layer?.borderColor = .white
    view.layer?.borderWidth = 2
	view.layer?.cornerRadius = 7
    return view
  }
  
  func setColor(color: NSColor) {
    layer?.backgroundColor = color.cgColor
  }
}
