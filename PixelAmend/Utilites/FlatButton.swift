//
//  FlatButton.swift
//  
//
//  Created by Gowtham on 29/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import AppKit

class FlatSegment: NSSegmentedControl {
init() {
    super.init(frame: NSZeroRect)
    addFilter()
}

required init?(coder: NSCoder) {
    super.init(coder: coder)
    addFilter()
}

func addFilter() {
    let colorFilter = CIFilter(name: "CIFalseColor")!
    colorFilter.setDefaults()
    colorFilter.setValue(CIColor(cgColor: NSColor.white.cgColor), forKey: "inputColor0")
    colorFilter.setValue(CIColor(cgColor: NSColor.black.cgColor), forKey: "inputColor1")
    self.contentFilters = [colorFilter]
}
}

public class FlatButton : NSButton {
 
	public var buttonColor: NSColor = NSColor.systemBlue
    public var onClickColor: NSColor = NSColor.systemBlue
    public var textColor: NSColor = NSColor.white
	public var cornerRadius: CGFloat = 0
    public var borderColor: NSColor = NSColor.systemBlue

    public override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)
        let rectanglePath = NSBezierPath(rect: NSRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height))
        var fillColor: NSColor
        var strokeColor: NSColor
        rectanglePath.fill()
        if self.isHighlighted {
            strokeColor = self.buttonColor
            fillColor = self.onClickColor
        } else {
            strokeColor = self.onClickColor
            fillColor = self.buttonColor
        }
     
        strokeColor.setStroke()
        rectanglePath.lineWidth = 5
        rectanglePath.stroke()
        fillColor.setFill()
        rectanglePath.fill()
        bezelStyle = .shadowlessSquare
     
		self.layer?.cornerRadius = self.cornerRadius
		self.layer?.borderColor = self.borderColor.cgColor
		self.layer?.borderWidth = 1.0
        let textRect = NSRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        let textTextContent = self.title
        let textStyle = NSMutableParagraphStyle()
        textStyle.alignment = .center
        
        
        let textFontAttributes : [ NSAttributedString.Key : Any ] = [
            .font: NSFont.systemFont(ofSize: 13),
            .foregroundColor: textColor,
          .paragraphStyle: textStyle
        ]

        let textTextHeight: CGFloat = textTextContent.boundingRect(with: NSSize(width: textRect.width, height: CGFloat.infinity), options: .usesLineFragmentOrigin, attributes: textFontAttributes).height
        let textTextRect: NSRect = NSRect(x: 0, y: -3 + ((textRect.height - textTextHeight) / 2), width: textRect.width, height: textTextHeight)
        NSGraphicsContext.saveGraphicsState()
        textTextContent.draw(in: textTextRect.offsetBy(dx: 0, dy: 3), withAttributes: textFontAttributes)
        NSGraphicsContext.restoreGraphicsState()
    }
}
