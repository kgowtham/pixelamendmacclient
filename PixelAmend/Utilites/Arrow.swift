//
//  ArrowView.swift
//  
//
//  Created by Gowtham on 13/09/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa

protocol arrowDelegate {
	func arrowEditingStarted(type:Drawing)
	func finishedEditing()
	func invalidateRect(shape:NSView)
}

struct ValidateDirection {
	var vertical: String
	var horizantal: String
}
struct DeltaPoints {
	var x: CGFloat
	var y: CGFloat
}
class Arrow : NSView {
	var arrowThickNess : CGFloat = 2.0
	var startPoint:NSPoint!
	var endPoint:NSPoint!
	var activeHandle:ResizeHandle?
	var handlePosition = Array(repeating: NSRect.zero, count: 8)
	var resizeHandles:[ResizeHandle]! = (NSMutableArray.init(capacity: 8) as! [ResizeHandle])
	var arrowDelegate:arrowDelegate?
	var drawingType:Drawing?
	var startPointView:NSView?
	var endPointView:NSView?
	private var cursorType: NSCursor {
		return .openHand
	}
	//MARK:
	override func draw(_ rect: CGRect) {
		let path = NSBezierPath()
		path.lineWidth = arrowThickNess
		path.move(to: startPoint)
		path.line(to: endPoint)
		let pointerLineLength = 10.0
		let diffX = endPoint.x - startPoint.x
		let diffY = endPoint.y - startPoint.y
		if (diffX > CGFloat(pointerLineLength) || diffX < CGFloat(-pointerLineLength) || diffY > CGFloat(pointerLineLength) || diffY < CGFloat(-pointerLineLength)) {
			path.move(to: endPoint)
			let arrowAngle = CGFloat(Double.pi / 4)
			let startEndAngle = atan((endPoint.y - startPoint.y) / (endPoint.x - startPoint.x)) + ((endPoint.x - startPoint.x) < 0 ? CGFloat(Double.pi) : 0)
			let arrowLine1 = CGPoint(x: endPoint.x + CGFloat(pointerLineLength) * cos(CGFloat(Double.pi) - startEndAngle + arrowAngle), y: endPoint.y - CGFloat(pointerLineLength) * sin(CGFloat(Double.pi) - startEndAngle + arrowAngle))
			let arrowLine2 = CGPoint(x: endPoint.x + CGFloat(pointerLineLength) * cos(CGFloat(Double.pi) - startEndAngle - arrowAngle), y: endPoint.y - CGFloat(pointerLineLength) * sin(CGFloat(Double.pi) - startEndAngle - arrowAngle))
			path.line(to: arrowLine1)
			path.move(to: endPoint)
			path.line(to: arrowLine2)
		}
		NSColor.systemBlue.set()
		path.stroke()

		let arrowLayer = CAShapeLayer()
		arrowLayer.name = "arrowLayer"
		arrowLayer.path = path.cgPath
		self.layer?.addSublayer(arrowLayer)
		
	}
	public func addClickGesture() {
		let clickGesture = NSClickGestureRecognizer.init(target: self, action: #selector(arrowShapeClicked(sender:)))
		clickGesture.numberOfClicksRequired = 1
		clickGesture.isEnabled = true
		self.addGestureRecognizer(clickGesture)
		self.setupTrackingArea()
	}
	@objc func arrowShapeClicked(sender:NSClickGestureRecognizer) {
		if NSCursor.current == NSCursor.openHand {
			createStartAndEndDot()
			arrowDelegate?.arrowEditingStarted(type: .arrow)
		}
	}
	private func setupTrackingArea() {
		let trackingArea = NSTrackingArea(rect: bounds, options: [.activeAlways, .mouseMoved, .mouseEnteredAndExited], owner: self, userInfo: nil)
		self.addTrackingArea(trackingArea)
	}
	override func mouseDragged(with event: NSEvent) {
		if drawingType == .arrowEditing {
			self.layer?.backgroundColor = NSColor.red.cgColor
			let deltaPoint = CGPoint(x: self.frame.origin.x + event.deltaX, y: self.frame.origin.y - event.deltaY)
			self.frame = NSRect(x:deltaPoint.x, y: deltaPoint.y, width: self.frame.size.width, height: self.frame.size.height)
		} else if(drawingType == .drawingStartPointChanged) {
//			let startPointTempRect = self.convert(self.startPoint, to: self.superview)
//			let endPointTempRect = self.convert(self.endPoint, to: self.superview)
//			self.frame = self.superview!.frame
//			self.startPoint = startPointTempRect
//			self.endPoint = endPointTempRect
//			self.layer?.backgroundColor = NSColor.red.cgColor
//			createStartAndEndDot()
		}
	}
	override func mouseUp(with event: NSEvent) {
		if drawingType == .arrowEditing {
			self.setNeedsDisplay(self.superview!.frame)
		}
	}
	override func mouseMoved(with event: NSEvent) {
        
		let point = event.locationInWindow
		let containedpoint = self.superview!.convert(point, to: self)
		for subview in self.subviews {
			let dotLayer = subview.layer
			if ((dotLayer!.name == "startDot" || dotLayer!.name == "endDot") && subview.frame.contains(containedpoint)) {
				print("frame contains point", subview.frame, dotLayer!.name!, containedpoint)
				if (dotLayer!.name == "startDot") {
					print("startpoint")
					drawingType = .drawingStartPointChanged
				} else {
					drawingType = .drawingEndPointChanged
				}
				NSCursor.resizeUpDown.push()
			}
		}
	}
	
	func createStartAndEndDot() {
        
		let startDot = NSView(frame: CGRect(x: self.startPoint.x - 6 , y: self.startPoint.y - 6, width: 6, height: 6))
		startDot.layer = CALayer()
		startDot.wantsLayer = true
		startDot.layer?.name = "startDot"
		startDot.layer?.backgroundColor = NSColor.black.cgColor
		startDot.layer?.cornerRadius = 3
		self.addSubview(startDot)
		
		let endDot = NSView(frame: CGRect(x: self.endPoint.x  , y: self.endPoint.y, width: 6, height: 6))
		endDot.layer = CALayer()
		endDot.wantsLayer = true
		endDot.layer?.backgroundColor = NSColor.black.cgColor
		endDot.layer?.name = "endDot"
		endDot.layer?.cornerRadius = 3
		self.addSubview(endDot)
	}
	
	func getArrowOrientaion() -> ValidateDirection {
		//vertically
		var verticalPostion = "";
		var horizantalPosition = "";
		if (self.startPoint.y > self.endPoint.y) { //vertically down
			verticalPostion = "down"
		}
		if (self.endPoint.y > self.startPoint.y ) { //vertically up
			verticalPostion = "up"
		}
		//horizantally
		if self.startPoint.x > self.endPoint.x { // horizantally left
			horizantalPosition = "left"
		}
		if self.endPoint.x > self.startPoint.x { //horizantally right
			horizantalPosition = "right"
		}
		// equal
		if self.endPoint.y == self.startPoint.y {
			verticalPostion = "equal"
		}
		if self.endPoint.x == self.startPoint.x {
			horizantalPosition = "equal"
		}
		return ValidateDirection(vertical: verticalPostion, horizantal: horizantalPosition)
	}
	func getViewPoint(direction: ValidateDirection, inputLayer: CALayer, containedPoint:NSPoint, arrowSubView:NSView) -> NSPoint {
		var updatedPoint = NSPoint(x: 0, y: 0)
		if direction.horizantal == "left" && direction.vertical == "down" { //start at end
			updatedPoint = NSPoint(x: self.frame.size.width - containedPoint.x, y: self.frame.size.height - containedPoint.y)
		}
		return updatedPoint
	}

}
class RectangleShape : NSView {
	
	override func draw(_ rect: CGRect) {
		//rectangle
		let path  =  NSBezierPath(rect: rect)
		NSColor.clear.setFill()
		path.fill()
		NSColor.clear.set()
		path.lineWidth  = 15
		path.stroke()
		self.layer?.sublayers?.forEach({ (rectLayer) in
			if (rectLayer.name == "rectLayer") {
				rectLayer.removeFromSuperlayer()
			}
		})
		let rectLayer = CAShapeLayer()
		rectLayer.name = "rectLayer"
		rectLayer.path = path.cgPath
		rectLayer.lineWidth = 3
        rectLayer.fillColor = NSColor.clear.cgColor
		rectLayer.strokeColor = NSColor.orange.cgColor
		rectLayer.backgroundColor = NSColor.blue.cgColor
		self.layer?.addSublayer(rectLayer)
	}
}
class CircleShape: NSView {
	override func draw(_ rect: CGRect) {
		// circle
		self.layer?.sublayers?.forEach({ (rectLayer) in
			if (rectLayer.name == "rounded") {
				rectLayer.removeFromSuperlayer()
			}
		})
		let borderWidth:CGFloat = 4.0
		let ovalPath = NSBezierPath(ovalIn: CGRect(x: rect.origin.x + borderWidth, y: rect.origin.y + borderWidth, width: rect.size.width - (2 * borderWidth), height: rect.size.height - (2 * borderWidth)))
        let circleLayer = CAShapeLayer()
        circleLayer.path = ovalPath.cgPath
		circleLayer.name = "rounded"
        circleLayer.fillColor = NSColor.clear.cgColor
        circleLayer.lineWidth = CGFloat(borderWidth)
        circleLayer.strokeColor = NSColor.black.cgColor
        self.layer?.addSublayer(circleLayer)
	}
}

