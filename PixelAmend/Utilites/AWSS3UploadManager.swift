//
//  AWSS3UploadManager.swift
//  PixelAmend
//
//  Created by Gowtham on 09/10/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation

typealias progressBlock = (_ progress: Double) -> Void
typealias completionBlock = (_ response: Any?, _ error: Error?) -> Void

let BUCKET_NAME = "pixelamendmedia"
let SCERECT_KEY = "gowtham"
let S3_UPLOAD_URL = "http://pixelamendmedia.s3.amazonaws.com"

enum HMACAlgorithm {
	case MD5, SHA1, SHA224, SHA256, SHA384, SHA512
	func toCCHmacAlgorithm() -> CCHmacAlgorithm {
		var result: Int = 0
		switch self {
		case .MD5:
			result = kCCHmacAlgMD5
		case .SHA1:
			result = kCCHmacAlgSHA1
		case .SHA224:
			result = kCCHmacAlgSHA224
		case .SHA256:
			result = kCCHmacAlgSHA256
		case .SHA384:
			result = kCCHmacAlgSHA384
		case .SHA512:
			result = kCCHmacAlgSHA512
		}
		return CCHmacAlgorithm(result)
	}

	func digestLength() -> Int {
		var result: CInt = 0
		switch self {
		case .MD5:
			result = CC_MD5_DIGEST_LENGTH
		case .SHA1:
			result = CC_SHA1_DIGEST_LENGTH
		case .SHA224:
			result = CC_SHA224_DIGEST_LENGTH
		case .SHA256:
			result = CC_SHA256_DIGEST_LENGTH
		case .SHA384:
			result = CC_SHA384_DIGEST_LENGTH
		case .SHA512:
			result = CC_SHA512_DIGEST_LENGTH
		}
		return Int(result)
	}
}

class AWSS3UploadManager {
	
	static let shared = AWSS3UploadManager()
	private init () {}
	private var observation: NSKeyValueObservation?

	func getDateInRFCFormat () -> String {
		let date = Date()
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss Z" //RFC2822-Format
		return dateFormatter.string(from: date)
	}
	public func uploadVideo(fileUrl:URL, fileName:String, contentType:String, progressStatus: progressBlock?, completionStatus: completionBlock?) {
		uploadfile(fileUrl: fileUrl, fileName: fileName, contentType: contentType) { (progress) in
			progressStatus!(progress)
		} completion: { (completion, error) in
			completionStatus!(completion, error)
		}
	}
	func getContentType(contentType: String) -> String {
		return contentType == "Image/png" ? "photos" : "videos";
	}
//	9246171789
	private func uploadfile(fileUrl: URL, fileName: String, contentType: String, progress: progressBlock?, completion: completionBlock?) {
		let error: Error? = nil
		var data: Data? = nil
		do {
			data = try NSData(contentsOfFile: fileUrl.path, options: []) as Data?
			if let error = error {
				print("request error: \(error)")
			}
			let PATH_TO_CREATE = fileName
			let dateString = getDateInRFCFormat()
			let httpMethod = "PUT"
			let resource = "/\(BUCKET_NAME)/\(PATH_TO_CREATE)"
			let signn = "\(httpMethod)\n\n\(contentType)\n\(dateString)\n\(resource)"
			var signature = signn.hmac(key: SCERECT_KEY)
			signature = signature.replacingOccurrences(of: "\n", with: "")
			let authAWS = "AWS ACCESS_KEY:\(signature)"
			let md5Base64 = data?.md5
			let uploadURL =  S3_UPLOAD_URL + "/" + getContentType(contentType: contentType) + "/" + fileName
			let originalRequest = NSMutableURLRequest(url: URL(string: uploadURL)!)
			originalRequest.httpMethod = "PUT"
			originalRequest.httpBody = data
			originalRequest.setValue(md5Base64, forHTTPHeaderField: "Content-MD5 ")
			originalRequest.setValue(contentType, forHTTPHeaderField: "Content-Type ")
			originalRequest.setValue(authAWS, forHTTPHeaderField: "Authorization ")
			originalRequest.setValue("public-read", forHTTPHeaderField: "x-amz-acl")
			let task = URLSession.shared.dataTask(with: originalRequest as URLRequest, completionHandler: { data, response, error in
				let httpResponse = response as? HTTPURLResponse
				if let httpResponse = httpResponse {
					completion!(httpResponse, error)
				}
			})
			task.resume()
			observation = task.progress.observe(\.fractionCompleted) { taskProgress, _ in
				progress!(taskProgress.fractionCompleted)
			}
		} catch {
			
		}
	}
}

