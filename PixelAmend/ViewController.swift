//
//  ViewController.swift
//  
//
//  Created by Gowtham on 13/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Cocoa
import AVKit

class ViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource, recordingPreferences {

    @IBOutlet weak var recordVideo: RecordingOptionsImageView! {
        didSet {
            recordVideo.handleTap = {
                UserSharedPreferences.shared.saveUserPreferences(key: "captureType", value: "screenRecording")
                let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
                let identifier = NSStoryboard.SceneIdentifier("RecordVideoViewController")
                guard let recordVideoViewController = storyboard.instantiateController(withIdentifier: identifier) as? RecordVideoViewController else {
                  fatalError("Why cant i find ViewController? - Check Main.storyboard")
                }
                recordVideoViewController.recordPreferenceObj = self
                self.view.window?.appearance = NSAppearance(named: .darkAqua)
                self.view.window?.contentViewController = recordVideoViewController
            }
        }
    }
    @IBOutlet weak var tableView: NSTableView!
    var originalColumns = [NSTableColumn]()
	var userVideos:[URL]?
    override func viewDidLoad() {
		super.viewDidLoad()
		userVideos = getUserVideos()
		tableView.delegate = self
		tableView.dataSource = self
		tableView.doubleAction = #selector(handleDoubleClick)
		tableView.selectionHighlightStyle = .none
		originalColumns = tableView.tableColumns
	}
	func didSavePreference(key: String, value: String) {
		UserSharedPreferences.shared.saveUserPreferences(key: key, value: value)
	}
	
    
	@IBAction func showVideoCaptureOptions(_ sender: Any) {
		UserSharedPreferences.shared.saveUserPreferences(key: "captureType", value: "screenRecording")
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("RecordVideoViewController")
		guard let recordVideoViewController = storyboard.instantiateController(withIdentifier: identifier) as? RecordVideoViewController else {
		  fatalError("Why cant i find ViewController? - Check Main.storyboard")
		}
		recordVideoViewController.recordPreferenceObj = self
		view.window?.appearance = NSAppearance(named: .darkAqua)
		view.window?.contentViewController = recordVideoViewController
		
	}
	@IBAction func showImageCaptureOptions(_ sender: Any) {
		UserSharedPreferences.shared.saveUserPreferences(key: "captureType", value: "imageRecording")
		UserSharedPreferences.shared.saveUserPreferences(key: "recordingMode", value: "selectedRegion")
		let _ = CaptureWindowController.init()
		self.view.window?.close()
	}
	
	@IBAction func didShowAudioCaptureOptions(_ sender: Any) {
		let xPos = 200
		let yPos = 100
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("AudioRecordingWindow")
		let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? AudioRecordingWindow
		let mainWindow = NSWindow(contentRect: NSMakeRect(CGFloat(xPos), CGFloat(yPos), NSScreen.main!.frame.size.width - 400, NSScreen.main!.frame.size.height - 200), styleMask: [.closable, .resizable, .miniaturizable], backing: .buffered, defer: false)
		mainWindow.titlebarAppearsTransparent = true
		mainWindow.contentViewController = viewcontroller
		mainWindow.makeKeyAndOrderFront(mainWindow)
		mainWindow.styleMask.remove(.titled)
		NSApp.activate(ignoringOtherApps: true)
		self.view.window?.close()

	}
	@IBAction func didShowFaceTimeVideoCaptureOptions(_ sender: Any) {
		let xPos = 200
		let yPos = 100
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("VideoRecordingWindow")
		let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? VideoRecordingWindow
		let mainWindow = NSWindow(contentRect: NSMakeRect(CGFloat(xPos), CGFloat(yPos), NSScreen.main!.frame.size.width - 400, NSScreen.main!.frame.size.height - 200), styleMask: [.closable, .resizable, .miniaturizable], backing: .buffered, defer: false)
		mainWindow.titlebarAppearsTransparent = true
		mainWindow.contentViewController = viewcontroller
		mainWindow.makeKeyAndOrderFront(mainWindow)
		mainWindow.styleMask.remove(.titled)
		NSApp.activate(ignoringOtherApps: true)

	}
	@objc func handleDoubleClick() {
        let clickedRow = tableView.clickedRow
		let asetUrl = userVideos![clickedRow]
		if (asetUrl.pathExtension == "png") {
			showImageEditor(imageUrl: asetUrl)
		} else {
			showVideoEditor(videoUrl: asetUrl)
		}
		self.view.window?.close()
	}
	func showImageEditor(imageUrl : URL) {
		let xPos = 200
		let yPos = 100
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("PostProcessingImageController")
		let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? PostProcessingImageController
		viewcontroller?.recordedImage = imageUrl
        let mainWindow = ClosableWindow(contentRect: NSMakeRect(CGFloat(xPos), CGFloat(yPos), NSScreen.main!.frame.size.width - 400, NSScreen.main!.frame.size.height - 200), styleMask: [.titled, .closable, .resizable, .miniaturizable], backing: .buffered, defer: false)
		mainWindow.contentViewController = viewcontroller
		mainWindow.makeKeyAndOrderFront(mainWindow)
		NSApp.activate(ignoringOtherApps: true)
	}

	func showVideoEditor(videoUrl: URL) {
		
		let xPos = 200
		let yPos = 100
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("PostProcessingVideoController")
		let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? PostProcessingVideoController
		viewcontroller?.recordedVideo = videoUrl
		let mainWindow = ClosableWindow(contentRect: NSMakeRect(CGFloat(xPos), CGFloat(yPos), 500, NSScreen.main!.frame.size.height - 200), styleMask: [.titled, .closable, .resizable, .miniaturizable], backing: .buffered, defer: false)
		mainWindow.contentViewController = viewcontroller
		mainWindow.makeKeyAndOrderFront(mainWindow)
		NSApp.activate(ignoringOtherApps: true)
		
	}
	override var representedObject: Any? {
		didSet {
			// Update the view, if already loaded.
		}
	}
	func getUserVideos() -> [URL]{
		return FileManager.default.urls(for: .documentDirectory, skipsHiddenFiles: true)!
	}
	func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
		let video = userVideos![row]
//		AWSS3UploadManager.shared.uploadVideo(fileUrl: video, fileName: video.lastPathComponent, contentType: "Image/png") { (progress) in
//			print("progress block", progress)
//		} completionStatus: { (status, error) in
//			print("video uploaded");
//		}
		let view = VideosDetailView()
        view.frame = NSRect(x: 0, y: 0, width: self.view.frame.size.width, height: 80)
		view.videoName.stringValue = video.lastPathComponent
		return view;
	}
    func numberOfRows(in tableView: NSTableView) -> Int {
		return userVideos!.count
		
	}
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
		return 80
	}
}
extension ViewController  {
  // MARK: Storyboard instantiation
	static func freshController() -> ViewController {
		let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		let identifier = NSStoryboard.SceneIdentifier("ViewController")
		guard let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? ViewController else {
				fatalError("Why cant i find ViewController? - Check Main.storyboard")
		}
		return viewcontroller
	}
}


class ClosableWindow: NSWindow {
	override func close() {
		self.orderOut(NSApp)
	}
}
class RecordingOptionsImageView : NSImageView {
    var handleTap: (() -> Void)? = nil

    override func mouseDown(with event: NSEvent) {
        let clickCount: Int = event.clickCount
        if clickCount == 1 {
            handleTap?()
            // User at least double clicked in image view
        }
    }
}
