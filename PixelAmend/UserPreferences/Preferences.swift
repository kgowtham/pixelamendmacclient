//
//  UserPreferences.swift
//  PixelAmend
//
//  Created by Gowtham on 26/10/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa

class UserSharedPreferences {
	static let shared = UserSharedPreferences()
	private init () {}
	func resetCoreData() {
		let appdelegate = NSApplication.shared.delegate as? AppDelegate
		let managedObjectContext = appdelegate!.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserPreferences")
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedObjectContext.fetch(fetchRequest)
			for managedObject in results {
				if let managedObjectData: NSManagedObject = managedObject as? NSManagedObject {
					managedObjectContext.delete(managedObjectData)
				}
			}
		} catch let error as NSError {
			print("delete failed", error)
		}
		
	}

	func fetchUserPreferences() -> NSManagedObject? {
		let appdelegate = NSApplication.shared.delegate as? AppDelegate
		let managedObjectContext = appdelegate!.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserPreferences")
		var resultData : NSManagedObject? =  nil;
		do {
			let result = try managedObjectContext.fetch(fetchRequest) as? [NSManagedObject]
			for data  in result! {
				resultData = data
			}
		} catch let error as NSError {
			print("couldn't save \(error) \(error.userInfo)")
		}
		return resultData;
	}
	func saveUserPreferences(key:String, value:Any) {
		guard let appdelegate = NSApplication.shared.delegate as? AppDelegate else{ return }
		let managedObjectContext = appdelegate.persistentContainer.viewContext
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserPreferences")
		fetchRequest.returnsObjectsAsFaults = false
		do {
			let results = try managedObjectContext.fetch(fetchRequest)
			if results.isEmpty {
				let userEntity = NSEntityDescription.entity(forEntityName: "UserPreferences", in: managedObjectContext)!
				let user = NSManagedObject(entity: userEntity, insertInto: managedObjectContext)
				user.setValue(value, forKey: key)
			} else {
				let user = results[0] as! NSManagedObject
				user.setValue(value, forKey: key)
			}
			do {
				try managedObjectContext.save()
			} catch let error as NSError {
				print("couldn't save \(error) \(error.userInfo)")
			}
		}  catch let error as NSError {
			print("delete failed", error)
		}

	}

}
