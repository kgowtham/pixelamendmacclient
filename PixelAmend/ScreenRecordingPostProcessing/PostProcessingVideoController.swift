//
//  PostProcessingVideoController.swift
//  
//
//  Created by Gowtham on 26/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa
import AVKit

class PostProcessingVideoController: NSViewController {
	
	var recordedVideo:URL?
	@IBOutlet weak var mediaEditorSuperView: NSView!
	@IBOutlet weak var videoPlayer: AVPlayerView!
	@IBOutlet weak var videoTitle: NSTextField!
	@IBOutlet weak var MediaEditingViewHeightConstraint: NSLayoutConstraint!
	var isCutVideo = false
	@IBOutlet weak var trimVideo: FlatButton! {
		didSet {
			trimVideo.buttonColor = NSColor.black
			trimVideo.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var cutVideo: FlatButton! {
		didSet {
			cutVideo.buttonColor = NSColor.black
			cutVideo.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var shareVideo: FlatButton! {
		didSet {
			shareVideo.cornerRadius = 5
		}
	}
	@IBOutlet weak var cutVideoTempButton: FlatButton!  {
		didSet {
			cutVideoTempButton.buttonColor = .systemBlue
			cutVideoTempButton.cornerRadius = 3
			cutVideoTempButton.borderColor = .systemBlue
			cutVideoTempButton.wantsLayer = false
			cutVideoTempButton.isHidden = true
		}
	}
	@IBOutlet weak var RotateVideo: FlatButton! {
		didSet {
			RotateVideo.buttonColor = NSColor.black
			RotateVideo.borderColor = NSColor.black
		}
	}
	@IBOutlet weak var ApplyFilter: FlatButton! {
	   didSet {
		   ApplyFilter.buttonColor = NSColor.black
		   ApplyFilter.borderColor = NSColor.black
	   }
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		videoPlayer.player = AVPlayer.init(url: recordedVideo!)
		videoTitle.stringValue = recordedVideo!.lastPathComponent
		MediaEditingViewHeightConstraint.constant = 0.0
	}
	@IBAction func didCutVideo(_ sender: Any) {
		cutVideoTempButton.isHidden = false
		isCutVideo = true
	}
	@IBAction func didTrimVideo(_ sender: Any) {
		mediaEditorSuperView.needsLayout = true
		cutVideoTempButton.isHidden = true
		isCutVideo = false
		videoPlayer.beginTrimming { result in
			self.cutVideoTempButton.isHidden = true
			if result == .okButton {
				self.getSelectedDurations()
			} else {
				
			}
		}
	}
	@IBAction func didRotateVideo(_ sender: Any) {
		let asset = AVAsset.init(url: recordedVideo!)
		rotateVideoByTransform(videoAsset: asset) { (url, error) in
		
		}
	}
	@IBAction func didAddfilter(_ sender: Any) {
		MediaEditingViewHeightConstraint.constant  = 140
//		applyBlurFilter()
	}
	func getSelectedDurations() {
		let startTime = videoPlayer.player?.currentItem?.reversePlaybackEndTime
		let endTime =   videoPlayer.player?.currentItem?.forwardPlaybackEndTime
		if (isCutVideo) {
			sliceVideoParts(startTime: startTime!, endTime: endTime!)
		} else {
			let preset = AVAssetExportPresetAppleM4V720pHD
			let exportSession = AVAssetExportSession(asset: (videoPlayer.player?.currentItem!.asset)!, presetName: preset)!
			exportSession.outputFileType = AVFileType.m4v
			exportSession.outputURL = getFilePath()
			let timeRange = CMTimeRangeFromTimeToTime(start: startTime!, end: endTime!)
			exportSession.timeRange = timeRange
			exportSession.exportAsynchronously {
				switch exportSession.status {
				case .completed:
					break
				case .failed:
					break
				default:
					break
				}
			}

		}
	}
	func getFilePath() -> URL {
		let filePath = self.captureTemporaryFilePath()
		if FileManager.default.fileExists(atPath: filePath) {
			do {
				try FileManager.default.removeItem(atPath: filePath)
			} catch  {
				
			}
		}
		return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(filePath)
	}
	func captureTemporaryFilePath() -> String {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "yyyy-MM-dd"
		let timeFormatter = DateFormatter()
		timeFormatter.dateFormat = "HH-mm-ss"
		let now = Date()
		let date = dateFormatter.string(from: now)
		let time = timeFormatter.string(from: now)
		let fileName = NSString.init(format: "%@%@-%@.%@",  "ScreenRecording", date, time, "mov")
		return fileName as String
	}

	func sliceVideoParts(startTime:CMTime, endTime:CMTime) {
		let assetDuration = videoPlayer.player?.currentItem!.asset.duration
		var outputUrl : [AVAsset]? = []
		if (startTime.seconds != 0) {
			let assetRange = CMTimeRangeFromTimeToTime(start: CMTime(value: 1 , timescale: 1), end: CMTime(value: CMTimeValue(startTime.seconds), timescale: 1))
			generateVideoForDuration(range: assetRange) { (url, error) in
				if (error == nil) {
					outputUrl?.append(AVAsset.init(url: url!))
					if (outputUrl!.count > 1) {
						self.mergeVideos(mergeVideos: outputUrl!) { (url, error) in
								print("outputurls");
						}
					}
				}
			}
		}
		if (endTime.seconds != assetDuration?.seconds) {
			let assetRange = CMTimeRangeFromTimeToTime(start: CMTime(value:CMTimeValue(endTime.seconds), timescale: 1), end: assetDuration!)
			generateVideoForDuration(range: assetRange) { (url, error) in
 				outputUrl?.append(AVAsset.init(url: url!))
				if (outputUrl!.count > 1) {
					self.mergeVideos(mergeVideos: outputUrl!) { (url, error) in
							print("outputurls");
					}
				}
			}
		}
	}
	func generateVideoForDuration(range:CMTimeRange, completion:@escaping (URL?, Error?) -> ()) {
		let preset = AVAssetExportPresetAppleM4V720pHD
		let exportSession = AVAssetExportSession(asset: (videoPlayer.player?.currentItem!.asset)!, presetName: preset)!
		exportSession.outputFileType = AVFileType.m4v
		exportSession.outputURL = getFilePath()
		exportSession.timeRange = range
		exportSession.exportAsynchronously {
			switch exportSession.status {
			case .completed:
				completion(exportSession.outputURL!, nil)
				break
			case .failed:
				completion(nil, exportSession.error)
				break
			default:
				break
			}
		}
	}
	func mergeVideos(mergeVideos:[AVAsset], completion:@escaping (URL?, Error?) -> ()) {
		let mainComposition = AVMutableComposition()
	  let compositionVideoTrack = mainComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
//	  let soundtrackTrack = mainComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
		var insertTime = CMTime.zero
	  for videoAsset in mergeVideos {
		try! compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .video)[0], at: insertTime)
//		if videoAsset.tracks(withMediaType: .audio).count > 0 {
//			try! soundtrackTrack?.insertTimeRange(CMTimeRangeMake(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .audio)[0], at: insertTime)
//		}
		insertTime = CMTimeAdd(insertTime, videoAsset.duration)
	  }
	 let outputFileURL = getFilePath()
	  let exporter = AVAssetExportSession(asset: mainComposition, presetName: AVAssetExportPresetHighestQuality)
	  exporter?.outputURL = outputFileURL
	  exporter?.outputFileType = AVFileType.m4v
		
	  exporter?.exportAsynchronously {
		if let url = exporter?.outputURL{
			completion(url, nil)
		}
		if let error = exporter?.error {
			completion(nil, error)
		}
	  }
	}
	func rotateVideoByTransform(videoAsset:AVAsset, completion:@escaping (URL?, Error?) -> ()) {
		let mainComposition = AVMutableComposition()
		let compositionVideoTrack = mainComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
		try! compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: .zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .video)[0], at: CMTime.zero)
		compositionVideoTrack?.preferredTransform = CGAffineTransform(rotationAngle: (.pi / 2 + .pi))
		let outputFileURL = getFilePath()
		let exporter = AVAssetExportSession(asset: mainComposition, presetName: AVAssetExportPresetHighestQuality)
		exporter?.outputURL = outputFileURL
		exporter?.outputFileType = AVFileType.m4v
		exporter?.exportAsynchronously {
			if let url = exporter?.outputURL{
				completion(url, nil)
			}
			if let error = exporter?.error {
				completion(nil, error)
			}
		}
	}
	func applyBlurFilter() {
		let asset  =  AVAsset(url: recordedVideo!)
		let filter = CIFilter(name: "CIGaussianBlur")!
		let composition = AVVideoComposition(asset: asset, applyingCIFiltersWithHandler: { request in
			// Clamp to avoid blurring transparent pixels at the image edges
			let source = request.sourceImage.clampedToExtent()
			filter.setValue(source, forKey: kCIInputImageKey)
			// Vary filter parameters based on video timing
			filter.setValue(3, forKey: kCIInputRadiusKey)
			// Crop the blurred output to the bounds of the original image
			let output = filter.outputImage!.cropped(to: request.sourceImage.extent)
			// Provide the filter output to the composition
			request.finish(with: output, context: nil)
		})
		
		videoPlayer.player?.currentItem?.videoComposition = composition
		videoPlayer.player?.play()

	}
}
