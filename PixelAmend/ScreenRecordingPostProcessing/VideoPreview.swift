//
//  VideoPreview.swift
//  
//
//  Created by Gowtham on 26/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa

class VideoPreviewBackgroundView : NSView {
	var backgroundColorLayer:CALayer?
	override init(frame frameRect: NSRect) {
		super.init(frame: frameRect)
		self.wantsLayer = true
		self.wantsLayer = true
		self.autoresizingMask = [.width, .height]
		self.autoresizesSubviews = true
		self.backgroundColorLayer = CALayer.init()
		self.backgroundColorLayer!.frame = NSRect(x: 0, y: 0, width: frameRect.size.width, height: frameRect.size.height);
		self.backgroundColorLayer?.bounds = self.bounds
		self.backgroundColorLayer?.backgroundColor = NSColor.init(white: 0.0, alpha: 1).cgColor
		self.layer?.addSublayer(self.backgroundColorLayer!)
	}
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
class VideoPreview : NSPanel {
	var bgView: VideoPreviewBackgroundView?
	override init(contentRect: NSRect, styleMask style: NSWindow.StyleMask, backing backingStoreType: NSWindow.BackingStoreType, defer flag: Bool) {
		super.init(contentRect: NSZeroRect, styleMask: style, backing: backingStoreType, defer: flag)
		self.bgView = VideoPreviewBackgroundView.init(frame: contentRect)
		self.bgView?.autoresizingMask = [.width, .height]
		self.contentView?.layer?.backgroundColor = .black
		self.contentView?.addSubview(self.bgView!)
	}
}
