//
//  VideoPostProcessing.swift
//  
//
//  Created by Gowtham on 26/08/20.
//  Copyright © 2020 Gowtham. All rights reserved.
//

import Foundation
import Cocoa

class VideoPostProcessing: NSWindowController {
	override init(window: NSWindow?) {
		let panel = VideoPreview.init(contentRect: NSScreen.main!.frame, styleMask: [ .titled, .resizable, .miniaturizable, .closable], backing: .buffered, defer: false)
		super.init(window: panel)
		self.window?.setFrame(NSScreen.main!.frame, display: true)
		self.showWindow(self)
		self.window?.backgroundColor = .blue
		self.window?.makeKeyAndOrderFront(self.window)
		panel.becomeKey()
	}
	override var acceptsFirstResponder: Bool {
		return true
	}
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}
}
